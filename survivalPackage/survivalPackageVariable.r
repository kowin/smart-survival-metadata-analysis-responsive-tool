
survivalPackageVariable <- function(filterOption) {
    variable <- NULL
    tryCatch({
        path <- paste0("survivalPackage/metadata/", filterOption, "Layer2.csv")
        variable <- read.csv(path,
            encoding = "utf8",
            strip.white = TRUE,
            stringsAsFactors = FALSE)
    }, warning = function(w) {
        survivalPackageVariableWarning <<- w
    }, error = function(e) {
        survivalPackageVariableError <<- e
    }, finally = {
        return(variable)
    })
}
