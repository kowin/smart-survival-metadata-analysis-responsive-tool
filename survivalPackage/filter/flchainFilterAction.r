
flchainFilterAction <- function(input, variable, level) {
    dataset <- survival::flchain

    dataset <- dplyr::mutate(dataset, flc10 = ifelse(flc.grp == 10, 1, 0))

    exResult <- firstFilterResult(dataset)

    discrete <- allCategoricalFilter(variable)
    exResult <- categoricalCheckboxFilter(input, discrete, exResult)

    return(exResult)
}
