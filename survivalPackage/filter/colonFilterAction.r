
colonFilterAction <- function(input, variable, level) {
    dataset <- survival::colon

    exResult <- firstFilterResult(dataset)

    discrete <- allCategoricalFilter(variable)
    exResult <- categoricalCheckboxFilter(input, discrete, exResult)

    return(exResult)
}
