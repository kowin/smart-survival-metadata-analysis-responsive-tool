
## Document
### https://vincentarelbundock.github.io/Rdatasets/doc/survival/flchain.html

flchainFilterOption <- function(variable, level) {
    discrete <- allCategoricalFilter(variable)
    ui <- categoricalCheckboxList(discrete, level)
    return(ui)
}
