
survivalPackageRawData <- function(option) {
    rawData <- switch(option,
        bladder     = survival::bladder,
        cancer      = survival::cancer,
        cgd         = survival::cgd,
        colon       = survival::colon,
        flchain     = survival::flchain,
        genfan      = survival::genfan,
        heart       = survival::heart,
        kidney      = survival::kidney,
        leukemia    = survival::leukemia,
        logan       = survival::logan,
        lung        = survival::lung,
        mgus        = survival::mgus,
        mgus2       = survival::mgus2,
        nwtco       = survival::nwtco,
        ovarian     = survival::ovarian,
        pbc         = survival::pbc,
        rats        = survival::rats,
        retinopathy = survival::retinopathy,
        stanford2   = survival::stanford2,
        tobin       = survival::tobin,
        transplant  = survival::transplant,
        veteran     = survival::veteran,
        NULL
    )
    return(rawData)
}
