
survivalPackageFilterAction <- function(input, variable, level) {
    if (is.null(variable) | is.null(level)) {
        return(list(NULL, list(NULL, NULL), NULL))
    }

    filteredDataset <- switch(input$embeddedChoicedDataset,
        colon = colonFilterAction(input, variable, level),
        flchain = flchainFilterAction(input, variable, level),
        list(NULL, list(NULL, NULL), NULL)
    )
    return(filteredDataset)
}
