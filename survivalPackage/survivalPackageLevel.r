
survivalPackageLevel <- function(filterOption) {
    level <- NULL
    tryCatch({
        path <- paste0("survivalPackage/metadata/", filterOption, "Layer3.csv")
        level <- read.csv(path,
            encoding = "utf8",
            strip.white = TRUE,
            stringsAsFactors = FALSE)
    }, warning = function(w) {
        survivalPackageLevelWarning <<- w
    }, error = function(e) {
        survivalPackageLevelError <<- e
    }, finally = {
        return(level)
    })
}
