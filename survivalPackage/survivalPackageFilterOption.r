
survivalPackageFilterOption <- function(filterOption, variable, level) {
    if (is.null(variable) | is.null(level)) {
        return(NULL)
    }

    ui <- switch(filterOption,
        colon = colonFilterOption(variable, level),
        flchain = flchainFilterOption(variable, level),
        NULL
    )
    return(ui)
}
