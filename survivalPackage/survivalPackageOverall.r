
survivalPackageOverall <- function(filterOption) {
    overall <- NULL
    tryCatch({
        path <- paste0("survivalPackage/metadata/", filterOption, "Layer1.csv")
        overall <- read.csv(path,
            encoding = "utf8",
            strip.white = TRUE,
            stringsAsFactors = FALSE)
    }, warning = function(w) {
        survivalPackageOverallWarning <<- w
    }, error = function(e) {
        survivalPackageOverallError <<- e
    }, finally = {
        return(overall)
    })
}
