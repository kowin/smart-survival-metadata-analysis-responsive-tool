
getPvalue <- function(formula, data) {
    sdiff <- survival::survdiff(formula, data = data)
    pvalue <- stats::pchisq(sdiff$chisq, length(sdiff$n) - 1, lower.tail = FALSE)
    return (pvalue)
}
