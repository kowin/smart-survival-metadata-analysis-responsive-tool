
dynamicCoxTableOption <- function(input, metadata, type) {
    variable <- metadata$Variable
    level <- metadata$Level

    if (is.null(variable) |
        is.null(level) |
        !is.null(variable["Error", "Error Message"])
    ) {
        return(NULL)
    }

    termList <- input[[paste0(type, "SurvivalFactorTermList")]]

    termInVariable <- dplyr::filter(variable, Colname %in% termList)
    nominal <- dplyr::filter(termInVariable, Type %in% c("Categorical", "Nominal"))
    if (0 == count(nominal)[[1]]) {
        return(NULL)
    }


    radioList <- tagList()
    for (row in 1:count(nominal)[[1]]) {
        id <- nominal[row, "Colname"]
        title <- nominal[row, "Label"]
        itemLevel <- dplyr::filter(level, Colname == id)
        radioId <- paste0(type, id, "CoxReferenceLevel")
        selected <- input[[radioId]]
        choices <- itemLevel$Label
        radio <- .radioButtonAll(radioId, title, selected, choices)
        radioList <- tagList(radioList, radio)
    }
    return(radioList)
}

.radioButtonAll <- function(id, title, selected, choices) {
    choicesLength <- length(choices)
    if (is.null(selected)) {
        selected <- ifelse(2 == choicesLength, choices[2], choices[1])
    }

    ui <- radioButtons(
        inputId   = id,
        label     = title,
        inline    = TRUE,
        selected  = selected,
        choices   = choices
    )
    return(ui)
}
