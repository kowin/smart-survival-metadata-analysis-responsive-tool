
transferCoxRowName <- function(coxModel, variable) {
    rowname <- c()

    rowname <- c()
    for (var in attr(coxModel$terms, "term.labels")) {
        variableLabel <- variable[variable$Colname == var, "Label"]
        if (var %in% names(coxModel$xlevels)) {
            for (c in 2:length(coxModel$xlevels[[var]])) {
                newLabel <- paste0(variableLabel, " (", coxModel$xlevels[[var]][c], " v.s. ", coxModel$xlevels[[var]][1], ")")
                rowname <- c(rowname, newLabel)
            }
        } else {
            rowname <- c(rowname, variableLabel)
        }
    }

    return(rowname)
}
