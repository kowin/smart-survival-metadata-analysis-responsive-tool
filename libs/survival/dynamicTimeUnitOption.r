
dynamicTimeUnitOption <- function(timeUnit, type, option = "default") {
    timeUnitUI <- selectizeInput(
        inputId   = paste0(type, "TimeUnit"),
        label     = "Time unit",
        selected  = timeUnit,
        choices   = c(
            "Day"   = "day",
            "Week"  = "week",
            "Month" = "month",
            "Year"  = "year"
        )
    )

    if ("year" == option) {
        timeOptionSelected <- .yearTimeOption(timeUnit)
    } else {
        timeOptionSelected <- .defaultTimeOption(timeUnit)
    }

    timeOptionChoices <- switch(timeUnit,
        day = .dayChoices(),
        week = .weekChoices(),
        month = .monthChoices(),
        year = .yearChoices(),
        NULL
    )

    timeOptionUI <- selectizeInput(
        inputId   = paste0(type, "IntervalType"),
        label     = "Time unit transformer",
        selected  = timeOptionSelected,
        choices   = timeOptionChoices
    )

    ui <- tagList(
        tags$div(class = "col-lg-4",
            timeUnitUI
        ),
        tags$div(class = "col-lg-8",
            timeOptionUI
        )
    )
    result <- list(timeUnitUI, timeOptionUI)
    return(result)
}

.yearTimeOption <- function(timeUnit) {
    timeOptionSelected <- switch(timeUnit,
        day = "/365",
        week = "*7 /365",
        month = "/12",
        year = "1",
        "1"
    )
    return(timeOptionSelected)
}

.defaultTimeOption <- function(timeUnit) {
    timeOptionSelected <- switch(timeUnit,
        day = "/365 *12",
        week = "1",
        month = "1",
        year = "1",
        "1"
    )
    return(timeOptionSelected)
}

.dayChoices <- function() {
    choices <- c(
        "no change" = "1",
        "Day to Week (/7)" = "/7",
        "Day to Month (/365 *12)" = "/365 *12",
        "Day to Year (/365)" = "/365")
    return(choices)
}

.weekChoices <- function() {
    choices <- c(
        "Week to Day (*7)" = "*7",
        "no change" = "1",
        "Week to Month (*7 /365 *12)" = "*7 /365 *12",
        "Week to Year (*7 /365)" = "*7 /365")
    return(choices)
}

.monthChoices <- function() {
    choices <- c(
        "Month to Day (/12 *365)" = "/12 *365",
        "Month to Week (/12 *365 /7)" = "/12 *365 /7",
        "no change" = "1",
        "Month to Year (/12)" = "/12")
    return(choices)
}

.yearChoices <- function() {
    choices <- c(
        "Year to Day (*365)" = "*365",
        "Year to Week (*365 /7)" = "*365 /7",
        "Year to Month (*12)" = "*12",
        "no change" = "1")
    return(choices)
}
