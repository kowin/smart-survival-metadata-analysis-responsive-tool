
dynamicFactorOption <- function(categoricalAxis, continuousAxis, dataset, type) {
    # When there are two or more identical categorical column,
    # the index of new axis will wrong.
    # For fix it, we collect all the index and then delete them.
    newCategoricalAxis <- categoricalAxis
    deleteIndex <- c()
    for (row in 1:length(categoricalAxis)) {
        if (1 == length(summary(dataset[,categoricalAxis[row]]))) {
            # newCategoricalAxis <- newCategoricalAxis[-row]
            deleteIndex <- c(deleteIndex, -row)
        }
    }
    if (!is.null(deleteIndex)) {
        newCategoricalAxis <- newCategoricalAxis[deleteIndex]
    }

    survivalFactorTerm <- selectizeInput(
        inputId   = paste0(type, "SurvivalFactorTermList"),
        label     = tags$h4("Right hand side of formula"),
        multiple  = TRUE,
        choices   = list(
            "categorical" = newCategoricalAxis,
            "continuous" = continuousAxis
        )
    )

    return(survivalFactorTerm)
}
