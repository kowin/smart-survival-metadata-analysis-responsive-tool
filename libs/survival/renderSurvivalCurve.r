
renderSurvivalCurve <- function (
    input, formula, dataset, type,
    durationUnit = "day",
    datasetName = ""
) {
    if (is.null(dataset)) {
        return(NULL)
    }

    if (0 == count(dataset)[[1]]) {
        return(NULL)
    }
    withProgress(
        message = 'Render survival curve.',
        detail = "Read input value.",
        value = 0,
        expr = {
            curveType <- input[[paste0(type, "CurveType")]]
            showCensorData <- input[[paste0(type, "ShowCensorData")]]
            showConfidenceInterval <- input[[paste0(type, "ShowConfidenceInterval")]]
            showPvalue <- input[[paste0(type, "ShowPvalue")]]
            showRiskTable <- input[[paste0(type, "ShowRiskTable")]]
            showLineType <- input[[paste0(type, "ShowLineType")]]
            intervalType <- input[[paste0(type, "IntervalType")]]

            # for survival::Surv
            incProgress(0.2, detail = paste("Calculate survival fit."))
            dataset$EventDeath <- ("Death" == dataset$EventDeath)

            survivalFit <- survival::survfit(
                formula = formula,
                data = dataset,
                conf.type = "log-log")

            survivalFitMinUpper <- min(survivalFit$upper, na.rm=TRUE)
            survivalFitMinLower <- min(survivalFit$lower, na.rm=TRUE)

            incProgress(0.2, detail = paste("Set curve parameters."))
            curveParams <- .defaultCurveParam()

            if (is.null(curveType)) {
            } else if ("KaplanMeier" == curveType) {
                verticalPosition <- 0.9
                if (survivalFitMinUpper > verticalPosition & survivalFitMinLower < verticalPosition) {
                    verticalPosition <- survivalFitMinLower - 0.1
                }
                curveParams$fun <- NULL
                curveParams$main <- "Kaplan-Meier Survival Curve"
                curveParams$ylab <- "Survival probability"
                curveParams$pval.coord <- c(0.9 * max(survivalFit$time), verticalPosition)
            } else if ("NelsonAalen" == curveType) {
                verticalPosition <- 0.1
                if ((1 - survivalFitMinUpper) < verticalPosition) {
                    verticalPosition <- (1 - survivalFitMinUpper) / 4
                }
                curveParams$fun <- "cumhaz"
                curveParams$main <- "Nelson-Aalen Survival Curve"
                curveParams$ylab <- "Cumulative hazard"
                curveParams$pval.coord <- c(0.9 * max(survivalFit$time), verticalPosition)
            }

            curveParams$censor <- .isShow(showCensorData, curveParams$censor)
            curveParams$conf.int <- .isShow(showConfidenceInterval, curveParams$conf.int)
            curveParams$pval <- .isShow(showPvalue, curveParams$pval)
            curveParams$risk.table <- .isShow(showRiskTable, curveParams$risk.table)

            if (!is.null(showLineType)) {
                curveParams$linetype <- showLineType
            }

            realDurationUnit <- .transferRealUnit(durationUnit, intervalType)
            if (!is.null(realDurationUnit)) {
                maxSurvivalTime <- max(survivalFit$time)
                if ("Month" == realDurationUnit & maxSurvivalTime < 144) {
                    curveParams$break.time.by <- 12
                } else if ("Year" == realDurationUnit & maxSurvivalTime < 12) {
                    curveParams$break.time.by <- 1
                } else if ("Week" == realDurationUnit & maxSurvivalTime < 48) {
                    curveParams$break.time.by <- 4
                }
            }

            curveParams$xlab <- .assembleXlable(realDurationUnit, datasetName)

            # pre-calculate p-value
            incProgress(0.2, detail = paste("Calculate p-value."))
            pvalue <- NULL
            if (length(levels(summary(survivalFit)$strata)) != 0) {
                pvalue <- getPvalue(formula, dataset)
            }

            incProgress(0.2, detail = paste("Render survival curve start."))
            curve <- .renderCurve(survivalFit, curveParams, pvalue)
            incProgress(0.2, detail = paste("Render survival curve down."))
        }
    )

    return(curve)
}

.renderCurve <- function(survivalFit, curveParams, pvalue) {
    curve <- ggSurvivalCurve(
        survivalFit,
        fun = curveParams$fun,
        size = 1.6, # line size
        linetype = curveParams$linetype,
        break.time.by = curveParams$break.time.by,
        conf.int = curveParams$conf.int,
        censor = curveParams$censor,
        pval = curveParams$pval,
        pval.coord = curveParams$pval.coord,
        pvalue = pvalue,
        risk.table = curveParams$risk.table,
        legend = curveParams$legend,
        legend.title = curveParams$legend.title,
        risk.table.title = curveParams$risk.table.title,
        font.family = "Helvetica",
        font.main = c(24, "plain", "black"),
        font.x = c(20, "plain", "black"),
        font.y = c(20, "plain", "black"),
        font.tickslab = c(16, "plain", "black"),
        font.legend = c(16, "plain", "black"),
        risk.table.fontsize = 6,
        pval.size = 6,
        surv.scale = "percent",
        xlab = curveParams$xlab,
        ylab = curveParams$ylab,
        main = curveParams$main
    )

    return(curve)
}

.defaultCurveParam <- function() {
    curveParams <- NULL
    curveParams$fun <- NULL
    curveParams$linetype <- 1
    curveParams$break.time.by <- NULL
    curveParams$conf.int <- FALSE
    curveParams$censor <- FALSE
    curveParams$pval <- TRUE
    curveParams$pval.coord <- c(NULL, NULL)
    curveParams$risk.table <- TRUE
    curveParams$legend <- "top"
    curveParams$legend.title <- ""
    curveParams$risk.table.title <- "Number of patients at risk:"
    curveParams$xlab <- "Time after diagnosis"
    curveParams$ylab <- ""
    curveParams$main <- ""
    return(curveParams)
}

.isShow <- function(showOrHide, default) {
    isShow <- default
    if (!is.null(showOrHide)) {
        isShow <- switch(showOrHide,
            show = TRUE,
            hide = FALSE,
            default
        )
    }
    return(isShow)
}

.assembleXlable <- function(realDurationUnit, datasetName = "") {
    name <- ""
    if ("" != datasetName) {
        name <- paste0("of ", datasetName)
    }
    xLable <- paste0("Time after diagnosis ", name, " (", realDurationUnit, ")")
    return(xLable)
}

.transferRealUnit <- function(durationUnit, type) {
    result <- NULL
    if (!is.null(durationUnit) && !is.null(type)) {
        result <- switch(durationUnit,
            day = .transferDayUnit(type),
            week = .transferWeekUnit(type),
            month = .transferMonthUnit(type),
            year = .transferYearUnit(type),
        NULL)
    }
}


.transferDayUnit <- function(type) {
    result <- NULL
    if ("/365 *12" == type) {
        result <- "Month"
    } else if ("1" == type) {
        result <- "Day"
    } else if ("/365" == type) {
        result <- "Year"
    } else if ("/7" == type) {
        result <- "Week"
    }
    return(result)
}

.transferWeekUnit <- function(type) {
    result <- NULL
    if ("1" == type) {
        result <- "Week"
    } else if ("*7" == type) {
        result <- "Day"
    } else if ("*7 /365 *12" == type) {
        result <- "Month"
    } else if ("*7 /365" == type) {
        result <- "Year"
    }
    return(result)
}

.transferMonthUnit <- function(type) {
    result <- NULL
    if ("1" == type) {
        result <- "Month"
    } else if ("/12" == type) {
        result <- "Year"
    } else if ("/12 *365" == type) {
        result <- "Day"
    } else if ("/12 *365 /7" == type) {
        result <- "Week"
    }
    return(result)
}

.transferYearUnit <- function(type) {
    result <- NULL
    if ("1" == type) {
        result <- "Year"
    } else if ("*12" == type) {
        result <- "Month"
    } else if ("*365" == type) {
        result <- "Day"
    } else if ("*365 /7" == type) {
        result <- "Week"
    }
    return(result)
}
