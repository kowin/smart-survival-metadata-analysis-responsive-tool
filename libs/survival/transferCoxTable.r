
transferCoxTable <- function(coxInfo, pValueList, logRankTest = NULL) {
    coxTable <- .twoDigitsCoxTable(coxInfo)
    coxTableHead <- c("HR","95% CI","P value")
    if (!is.null(logRankTest)) {
        coxTableHead <- c(coxTableHead, "Log-rank test")
    }

    if (is.null(dim(coxTable))) {
        coxTable["HR"] <- coxTable["exp(coef)"]
        coxTable["95% CI"] <- paste0(coxTable["lower .95"], " - ", coxTable["upper .95"])
        coxTable["P value"] <- showPvalue(pValueList)

        if (!is.null(logRankTest)) {
            coxTable["Log-rank test"] <- logRankTest
        }

        coxTable <- coxTable[coxTableHead]
        coxTable <- t(as.matrix(coxTable))
    } else {
        ci <- cbind(coxTable[,"lower .95"], coxTable[,"upper .95"])
        hazardRatioConfidenceInterval <- apply(
            X = ci,
            MARGIN = 1,
            FUN = function(x) {
                paste0(x[1], " - ", x[2])
            }
        )
        coxTable <- cbind(
            coxTable[,"exp(coef)"],
            hazardRatioConfidenceInterval,
            showPvalue(pValueList)
        )

        if (!is.null(logRankTest)) {
            logrankList <- vector(mode = "character", length = dim(coxTable)[[1]])
            logrankList[1] <- logRankTest
            coxTable <- cbind(coxTable, logrankList)
        }

        colnames(coxTable) <- coxTableHead
    }
    return(coxTable)
}

.twoDigitsCoxTable <- function(coxTable) {
    if (is.null(dim(coxTable))) {
        twoDigitsCoxTable <- twoDigits(coxTable)
        names(twoDigitsCoxTable) <- names(coxTable)
    } else {
        twoDigitsCoxTable <- apply(
            X = coxTable,
            MARGIN = c(1, 2),
            FUN = function(x) {
                twoDigits(x)
            }
        )
    }
    return(twoDigitsCoxTable)
}
