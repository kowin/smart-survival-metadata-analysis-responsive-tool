
isShowSurvivalCurve <- function(input, dataset, variable, type) {
    showCurve <- input[[paste0(type, "ShowSurvivalCurve")]]
    if (!is.null(showCurve)) {
        if ("hide" == showCurve) {
            return(FALSE)
        }
    }

    termList <- input[[paste0(type, "SurvivalFactorTermList")]]
    product <- 1
    for (term in termList) {
        if (!(variable[variable$Colname == term, "Type"] %in% c("Categorical", "Nominal", "Ordinal"))) {
            return(FALSE)
        }
        factorCount <- length(summary(factor(dataset[,term])))
        if (0 != factorCount) {
            product <- product * factorCount
        }
        if (product > 6) {
            return(FALSE)
        }
    }
    return(TRUE)
}

