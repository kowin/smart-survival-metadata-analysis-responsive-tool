
showUniVariateCoxTable <- function(input, duration, event, dataset, variable, type) {
    if (is.null(dataset) |
        is.null(variable) |
        !is.null(variable["Error", "Error Message"])
    ) {
        return(NULL)
    }

    # for survival::Surv
    dataset$EventDeath <- ("Death" == dataset$EventDeath)

    newDataset <- relevelDataset(input, dataset, type)

    uniTermCoxTable <- NULL
    termList <- input[[paste0(type, "SurvivalFactorTermList")]]
    withProgress(
        message = 'Calculate uni-variate cox table.',
        detail = "Count size of term list.",
        value = 0,
        expr = {
            termListLength <- length(termList)
            unitSize <- 1 / termListLength
            for (term in termList) {
                incProgress(
                    amount = unitSize,
                    detail = paste0("Calculate cox table of ", term)
                )
                formula <- paste0("Surv(", duration, ", ", event, ") ~ ", term)
                survivalFormula <- eval(parse(text = formula))
                coxModel <- coxph(survivalFormula, newDataset)
                coxModelSummary <- summary(coxModel)
                coxInfo <- coxModelSummary$conf.int[,c("exp(coef)", "lower .95", "upper .95")]
                if (is.null(coxInfo)) {
                    next
                }

                logRankTest <- round(as.numeric(coxModelSummary$sctest["test"]), 2)
                coxTable <- transferCoxTable(
                    coxInfo = coxInfo,
                    pValueList = coxModelSummary$coefficients[,c("Pr(>|z|)")],
                    logRankTest = logRankTest
                )

                rowname <- transferCoxRowName(coxModel, variable)
                rownames(coxTable) <- rowname

                uniTermCoxTable <- rbind(uniTermCoxTable, coxTable)
            }
        }
    )
    return(uniTermCoxTable)
}
