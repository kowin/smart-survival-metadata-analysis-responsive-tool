
showMultiVariateCoxTable <- function(input, survivalFormula, dataset, variable, type) {
    if (is.null(dataset) |
        is.null(variable) |
        !is.null(variable["Error", "Error Message"])
    ) {
        return(list(NULL, NULL))
    }

    # DO NOT show multi-variate if there just only one term
    termList <- input[[paste0(type, "SurvivalFactorTermList")]]
    if (length(termList) < 2) {
        return(list(NULL, NULL))
    }

    # for survival::Surv
    dataset$EventDeath <- ("Death" == dataset$EventDeath)

    newDataset <- relevelDataset(input, dataset, type)

	coxModel <- coxph(survivalFormula, newDataset)
    coxModelSummary <- summary(coxModel)
    coxInfo <- coxModelSummary$conf.int[,c("exp(coef)", "lower .95", "upper .95")]
    if (is.null(coxInfo)) {
        return(list(NULL, NULL))
    }

    coxTable <- transferCoxTable(coxInfo, coxModelSummary$coefficients[,c("Pr(>|z|)")])

    rowname <- transferCoxRowName(coxModel, variable)
    rownames(coxTable) <- rowname

    logrankTest <- round(as.numeric(coxModelSummary$sctest["test"]), 2)
    result <- list(coxTable, logrankTest)
    return(result)
}
