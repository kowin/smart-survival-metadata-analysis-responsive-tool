
relevelDataset <- function(input, dataset, type) {
    if (is.null(dataset)) {
        return(NULL)
    }

    termList <- input[[paste0(type, "SurvivalFactorTermList")]]
    for (term in termList) {
        baseLevel <- input[[paste0(type, term, "CoxReferenceLevel")]]
        if (is.null(baseLevel)) {
            next
        }

        if (!is.ordered(dataset[,term])) {
            if (baseLevel %in% names(summary(dataset[,term]))) {
                dataset[,term] <- relevel(dataset[,term], ref = baseLevel)
            }
        }
    }
    return(dataset)
}
