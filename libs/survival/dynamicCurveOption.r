
dynamicCurveOption <- function(isShow, type) {
    # curveType <- radioButtons(
    #     inputId   = paste0(type, "CurveType"),
    #     label     = "Survival Analysis",
    #     inline    = TRUE,
    #     selected  = "KaplanMeier",
    #     choices   = c("Kaplan-Meier Survival Curve" = "KaplanMeier"
    #                 , "Nelson-Aalen Survival Curve" = "NelsonAalen")
    # )

    curveType <- buttonTypeRadioButton(
        inputId   = paste0(type, "CurveType"),
        label     = tags$h6("Curve Type"),
        inline    = FALSE,
        selected  = "KaplanMeier",
        choices   = c(
            "Kaplan-Meier" = "KaplanMeier",
            "Nelson-Aalen" = "NelsonAalen"
        ),
        specialClass = " btn-group curveType"
    )

    showCensorData <- buttonTypeRadioButton(
        inputId   = paste0(type, "ShowCensorData"),
        label     = tags$h6("Censor Data"),
        inline    = TRUE,
        selected  = "hide",
        choices   = c(
            "Show" = "show",
            "Hide" = "hide"
        )
    )

    showConfidenceInterval <- buttonTypeRadioButton(
        inputId   = paste0(type, "ShowConfidenceInterval"),
        label     = tags$h6("CI 95%"),
        inline    = TRUE,
        selected  = "hide",
        choices   = c(
            "Show" = "show",
            "Hide" = "hide"
        )
    )

    showPvalue <- buttonTypeRadioButton(
        inputId   = paste0(type, "ShowPvalue"),
        label     = tags$h6("P value"),
        inline    = TRUE,
        selected  = "show",
        choices   = c(
            "Show" = "show",
            "Hide" = "hide"
        )
    )

    showRiskTable <- buttonTypeRadioButton(
        inputId   = paste0(type, "ShowRiskTable"),
        label     = tags$h6("Risk Table"),
        inline    = TRUE,
        selected  = "show",
        choices   = c(
            "Show" = "show",
            "Hide" = "hide"
        )
    )

    showLineType <- buttonTypeRadioButton(
        inputId   = paste0(type, "ShowLineType"),
        label     = tags$h6("Line Type"),
        inline    = TRUE,
        selected  = "strata",
        choices   = c("solid", "strata")
    )

    showSurvivalCurve <- buttonTypeRadioButton(
        inputId   = paste0(type, "ShowSurvivalCurve"),
        label     = tags$h6("Survival Curve"),
        inline    = TRUE,
        selected  = isShow,
        choices   = c(
            "Show" = "show",
            "Hide" = "hide"
        )
    )

    showDatasetName <- textInput(
        inputId   = paste0(type, "ShowDatasetName"),
        label     = tags$h6("Show Dataset Name")
    )

    showCurveHeight <- sliderInput(
        inputId   = paste0(type, "ShowCurveHeight"),
        label     = tags$h6("Show Curve Height"),
        min       = 400,
        max       = 800,
        value     = 550,
        step      = 25
    )

    if (!is.null(isShow)) {
        if ("hide" == isShow) {
            ui <- showSurvivalCurve
            return(ui)
        }
    }

    ui <- tagList(
        tags$div(class = "col-lg-12", curveType),
        tags$div(class = "col-lg-6", showCensorData),
        tags$div(class = "col-lg-6", showPvalue),
        tags$div(class = "col-lg-6", showRiskTable),
        tags$div(class = "col-lg-6", showConfidenceInterval),
        tags$div(class = "col-lg-6", showLineType),
        tags$div(class = "col-lg-6", showSurvivalCurve),
        tags$div(class = "col-lg-12", showDatasetName),
        tags$div(class = "col-lg-12", showCurveHeight)
    )
    return(ui)
}
