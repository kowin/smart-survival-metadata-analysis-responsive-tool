
dynamicSurvivalFormula <- function(input, type) {
    intervalType <- input[[paste0(type, "IntervalType")]]
    termList <- input[[paste0(type, "SurvivalFactorTermList")]]

    duration <- "TimeOS"
    if (!is.null(intervalType)) {
        if ("1" != intervalType) {
            duration <- paste0(duration, intervalType)
        }
    }

    survivalFactor <- .termListToSurvivalFactor(termList)

    survivalFormula <- paste0("Surv(", duration, ", EventDeath) ~ ", survivalFactor)

    return(survivalFormula)
}

.termListToSurvivalFactor <- function(termList) {
    survivalFactor <- paste(termList, collapse = " + ")
    if ("" == survivalFactor) {
        survivalFactor <- "1"
    }
    return(survivalFactor)
}
