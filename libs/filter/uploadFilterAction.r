
uploadFilterAction <- function(input, dataset, variable, level) {
    if (is.null(dataset) |
        is.null(variable) |
        is.null(level) |
        !is.null(variable["Error", "Error Message"])
    ) {
        return(NULL)
    }

    exResult <- firstFilterResult(dataset)

    categorical <- allCategoricalFilter(variable)

    exResult <- categoricalCheckboxFilter(input, categorical, exResult, "upload")
    return(exResult)
}
