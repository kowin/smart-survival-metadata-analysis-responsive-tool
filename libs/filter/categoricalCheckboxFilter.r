
categoricalCheckboxFilter <- function(input, categorical, exResult, type = "") {
    withProgress(
        message = 'Filter categorical option list.',
        detail = "Count size of categorical list.",
        value = 0,
        expr = {
            categoricalSize <- count(categorical)[[1]]
            unitSize <- 1 / categoricalSize
            for (row in 1:categoricalSize) {
                colname <- categorical[row, "Colname"]
                title <- categorical[row, "Label"]
                incProgress(
                    amount = unitSize,
                    detail = paste("Filter ", title, " option.")
                )
                valueList <- input[[paste0(type, colname, "Checkbox")]]
                if ("all options" %in% valueList) {
                    next
                }
                expression <- categorical[row, "Label"]
                exResult <- checkboxFilter(exResult, colname, valueList, expression)
            }
        }
    )
    return(exResult)
}
