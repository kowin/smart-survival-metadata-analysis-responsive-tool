
allCategoricalFilter <- function(variable) {
    categorical <- dplyr::filter(variable, Type %in% c("Categorical", "Nominal", "Ordinal"))
    return(categorical)
}
