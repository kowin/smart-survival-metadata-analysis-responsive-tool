
recordFilterNote <- function(filterNote, filterExpression, beforeFilterCount, afterFilterCount, differenceCount) {
    filterTable <- filterNote[[1]]
    filterCount <- filterNote[[2]]

    filterCount <- filterCount + 1
    filterTable[filterCount, "Expression"] <- filterExpression
    filterTable[filterCount, "Before"] <- beforeFilterCount
    filterTable[filterCount, "Difference"] <- differenceCount
    filterTable[filterCount, "After"] <- afterFilterCount

    result <- list(filterTable, filterCount)
    return(result)
}
