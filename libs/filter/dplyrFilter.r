
dplyrFilter <- function(exResult, filterExpression, expression = NULL) {
    dataset <- exResult[[1]]
    filterNote <- exResult[[2]]

    realExpression <- paste0("dataset %>% dplyr::filter(", filterExpression, ")")
    beforeFilterCount <- count(dataset)[[1]]
    dataset <- eval(parse(text = realExpression))
    afterFilterCount <- count(dataset)[[1]]
    differenceCount <- beforeFilterCount - afterFilterCount

    if (is.null(expression)) {
        expression <- filterExpression
    }
    filterNote <- recordFilterNote(filterNote, expression, beforeFilterCount, afterFilterCount, differenceCount)

    result <- list(dataset, filterNote, afterFilterCount)
    return(result)
}
