
checkboxFilter <- function(exResult, filterColumn, valueList, expression = NULL) {
    tmpList <- paste(valueList, collapse = "\",\"")
    filterExpression <- paste0(filterColumn, " %in% c(\"", tmpList, "\")")
    return(dplyrFilter(exResult, filterExpression, expression))
}
