
categoricalCheckboxList <- function(categorical, level, type = "", defaultAll = FALSE) {
    checkboxList <- tagList()
    withProgress(
        message = 'Convert to categorical checkbox option list.',
        detail = "Count size of categorical list.",
        value = 0,
        expr = {
            categoricalSize <- count(categorical)[[1]]
            unitSize <- 1 / categoricalSize
            for (row in 1:categoricalSize) {
                id <- categorical[row, "Colname"]
                title <- categorical[row, "Label"]
                incProgress(
                    amount = unitSize,
                    detail = paste("Convert ", title, " checkbox option list.")
                )
                itemLabel <- dplyr::filter(level, Colname == id)
                choicesKey <- c("all options", itemLabel$Value)
                choicesValue <- c("all options", itemLabel$Label)
                choices <- setNames(choicesKey, choicesValue)
                checkboxId <- paste0(type, id, "Checkbox")
                if (defaultAll) {
                    defaultSelected <- choicesKey
                } else {
                    defaultSelected <- itemLabel$Value
                }
                checkbox <- .checkboxAll(checkboxId, title, defaultSelected, choices)
                checkboxList <- tagList(checkboxList, checkbox)
            }
        }
    )
    return(checkboxList)
}

.checkboxAll <- function(id, title, selected, choices) {
    ui <- checkboxGroupInput(
        inputId   = id,
        label     = title,
        inline    = TRUE,
        selected  = selected,
        choices   = choices
    )
    return(ui)
}
