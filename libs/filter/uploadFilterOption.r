
uploadFilterOption <- function(variable, level, defaultAll = FALSE) {
    if (is.null(variable) |
        is.null(level) |
        !is.null(variable["Error", "Error Message"])
    ) {
        return(NULL)
    }

    categorical <- allCategoricalFilter(variable)

    ui <- categoricalCheckboxList(categorical, level, "upload", defaultAll)
    return(ui)
}
