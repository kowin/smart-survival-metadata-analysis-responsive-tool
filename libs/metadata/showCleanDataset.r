
showCleanDataset <- function (input, dataset, variable, type) {
    data <- dataset[,input[[paste0(type, "CleanDataShowVariableCheckbox")]]]
    showVariableType <- input[[paste0(type, "CleanDataShowVariableType")]]
    if ("Colname" != showVariableType) {
        selected <- input[[paste0(type, "CleanDataShowVariableCheckbox")]]
        colnames(data) <- variable[variable$Colname %in% selected, showVariableType]
    }
    return(data)
}
