
fillInVariableMetadataContent <- function (input, metadataColnames) {
    table <- data.frame()
    counter <- 1
    for (colname in metadataColnames) {
        index <- sprintf("%03d", counter)
        table[counter, "Colname"] <- colname
        table[counter, "Type"] <- input[[paste0("fillInVariableType", index)]]
        table[counter, "Label"] <- input[[paste0("fillInVariableLabel", index)]]
        counter <- counter + 1
    }

    return(table)
}
