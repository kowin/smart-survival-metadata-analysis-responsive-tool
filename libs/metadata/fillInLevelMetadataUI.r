
fillInLevelMetadataUI <- function (categoricalColnames, dataset) {
    rowList <- tagList()
    withProgress(
        message = 'Make fill in UI of layer 3 metadata.',
        detail = "Count size of categorical columns.",
        value = 0,
        expr = {
            categoricalColnamesLength <- length(categoricalColnames)
            unitSize <- 1 / categoricalColnamesLength
            counter <- 1
            for (colname in categoricalColnames) {
                incProgress(
                    amount = unitSize,
                    detail = paste0("Make fill in UI of ", colname)
                )
                defaultValue <- levels(factor(dataset[,colname]))
                # transfer c(0,1) to c(1,0), and mapping it into c("yes", "no")
                if (2 == length(defaultValue) & 0 == defaultValue[1] & 1 == defaultValue[2]) {
                    defaultValue <- c(1,0)
                    defaultLabel <- c("yes", "no")
                } else {
                    defaultLabel <- NULL
                }

                withProgress(
                    message = paste0("Make fill in UI of ", colname),
                    detail = "Count size of default values.",
                    value = 0,
                    expr = {
                        defaultValueLength <- length(defaultValue)
                        maxLengthOfValue <- 100
                        if (defaultValueLength > maxLengthOfValue) {
                            rowList <- tags$div(
                                class = "h2",
                                paste0(
                                    "This column (", colname, ") contains over ",
                                    maxLengthOfValue, " different items, ",
                                    "please assign type of ", colname,
                                    " to continuous. Thank you."
                                )
                            )
                            break
                        }
                        innerUnitSize <- 1 / defaultValueLength
                        for (row in 1:length(defaultValue)) {
                            value <- defaultValue[row]
                            label <- ifelse(is.null(defaultLabel), value, defaultLabel[row])
                            incProgress(
                                amount = innerUnitSize,
                                detail = paste0("Make fill in UI at ", label)
                            )
                            index <- sprintf("%03d", counter)
                            row <- tags$div(class = "row",
                                tags$div(class = "col-sm-4 biggerFont",colname),
                                tags$div(class = "col-sm-4",value),
                                tags$div(class = "col-sm-4",.fillInLableOfLevel(index, colname, label))
                            )
                            rowList <- tagList(rowList, row)
                            counter <- counter + 1
                        }
                    }
                )
            }
        }
    )

    inputTable <- tagList(
        tags$div(class = "row",
            tags$div(class = "col-sm-4 h4","Colname"),
            tags$div(class = "col-sm-4 h4","Value"),
            tags$div(class = "col-sm-4 h4","Label")
        ),
        tags$hr(),
        rowList
    )

    return(inputTable)
}

.fillInLableOfLevel <- function(index, colname, value) {
    ui <- textInput(
        inputId   = paste0("fillInLableOfLevelValue", index),
        label     = " ",
        value     = value
    )
    return(ui)
}
