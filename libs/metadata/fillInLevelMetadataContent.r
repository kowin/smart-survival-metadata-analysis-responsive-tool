
fillInLevelMetadataContent <- function (input, categoricalColnames, dataset) {
    table <- data.frame()
    counter <- 1
    for (colname in categoricalColnames) {
        defaultValue <- levels(factor(dataset[,colname]))
        if (2 == length(defaultValue) & 0 == defaultValue[1] & 1 == defaultValue[2]) {
            defaultValue <- c(1,0)
        }
        for (value in defaultValue) {
            index <- sprintf("%03d", counter)
            table[counter, "Colname"] <- colname
            table[counter, "Value"] <- value
            table[counter, "Label"] <- input[[paste0("fillInLableOfLevelValue", index)]]
            counter <- counter + 1
        }
    }
    return(table)
}
