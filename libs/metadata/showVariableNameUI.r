
showVariableNameUI <- function (input, variable, type) {
    if (is.null(variable) | !is.null(variable["Error", "Error Message"])) {
        return(NULL)
    }

    variableType <- input[[paste0(type, "CleanDataShowVariableType")]]
    choices <- setNames(variable$Colname, variable[,variableType])

    if (is.null(input[[paste0(type, "CleanDataShowVariableCheckbox")]])) {
        selected <- variable$Colname
    } else {
        selected <- input[[paste0(type, "CleanDataShowVariableCheckbox")]]
    }

    ui <- checkboxGroupInput(
        inputId   = paste0(type, "CleanDataShowVariableCheckbox"),
        label     = "Choose variable",
        inline    = FALSE,
        selected  = selected,
        choices   = choices
    )

    return(ui)
}
