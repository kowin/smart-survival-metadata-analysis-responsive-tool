
fillInDownloadUI <- function (cleanDatasetName, layer) {
    name <- ifelse("" == cleanDatasetName, "your", cleanDatasetName)
    downloadUI <- downloadButton(
        outputId  = paste0("downloadYour", layer, "Metadata"),
        label = tags$span(paste(name, layer, "Metadata"), class = "h4"),
        class = "btn-primary btn-block"
    )

    return(downloadUI)
}
