
appendOriginLevel <- function (rawData, variable, level) {
    if (is.null(rawData) |
        is.null(variable) |
        !is.null(rawData["Error", "Error Message"]) |
        !is.null(variable["Error", "Error Message"])
    ) {
        return(NULL)
    }


    categorical <- variable$Colname[variable$Type %in% c("Categorical", "Nominal", "Ordinal")]
    lostLabelList <- categorical[!(categorical %in% level$Colname)]

    levelCount <- dim(level)[[1]]
    for (lostVariable in lostLabelList) {
        originLabel <- levels(factor(rawData[,lostVariable]))
        for (label in originLabel) {
            levelCount <- levelCount + 1
            level[levelCount, "Colname"] <- lostVariable
            level[levelCount, "Value"] <- label
            level[levelCount, "Label"] <- label
        }
    }

    levelCount <- levelCount + 1
    level[levelCount, "Colname"] <- "EventDeath"
    level[levelCount, "Value"] <- "1"
    level[levelCount, "Label"] <- "Death"

    levelCount <- levelCount + 1
    level[levelCount, "Colname"] <- "EventDeath"
    level[levelCount, "Value"] <- "0"
    level[levelCount, "Label"] <- "Censored"

    return(level)
}
