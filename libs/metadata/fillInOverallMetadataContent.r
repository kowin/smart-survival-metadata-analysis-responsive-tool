
fillInOverallMetadataContent <- function (input) {
    table <- data.frame()
    table[1, "Standard"] <- "TimeOS"
    table[1, "Colname"] <- input$fillInOverallTimeOS
    table[2, "Standard"] <- "EventDeath"
    table[2, "Colname"] <- input$fillInOverallEventDeath
    return(table)
}
