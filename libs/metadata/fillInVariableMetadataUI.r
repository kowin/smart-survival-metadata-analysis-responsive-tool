
fillInVariableMetadataUI <- function (metadataColnames) {
    rowList <- tagList()
    counter <- 1
    for (colname in metadataColnames) {
        index <- sprintf("%03d", counter)
        row <- tags$div(class = "row",
            tags$div(class = "col-sm-4 biggerFont", colname),
            tags$div(class = "col-sm-4", .selectVariableType(index)),
            tags$div(class = "col-sm-4", .fillInVariableLabel(index, colname))
        )
        rowList <- tagList(rowList, row)
        counter <- counter + 1
    }

    inputTable <- tagList(
        tags$div(class = "row",
            tags$div(class = "col-sm-4 h4", "Colname"),
            tags$div(class = "col-sm-4 h4", "Type"),
            tags$div(class = "col-sm-4 h4", "Label")
        ),
        tags$hr(),
        rowList
    )

    return(inputTable)
}

.selectVariableType <- function(index) {
    typeUI <- selectizeInput(
        inputId   = paste0("fillInVariableType", index),
        label     = " ",
        selected  = " ",
        choices   = list(
            " " = c(
                "Undefined" = " ",
                "Categorical" = "Categorical",
                "Continuous" = "Continuous"
            ),
            "Categorical" = c("Nominal", "Ordinal"),
            "Continuous" = c("Interval", "Ratio")
        )
    )
    return(typeUI)
}

.fillInVariableLabel <- function(index, colname) {
    ui <- textInput(
        inputId   = paste0("fillInVariableLabel", index),
        label     = " ",
        value     = colname
    )
    return(ui)
}
