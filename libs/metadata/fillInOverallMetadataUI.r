
fillInOverallMetadataUI <- function (metadataColnames) {
    timeUI <- selectizeInput(
        inputId   = "fillInOverallTimeOS",
        label     = " ",
        choices   = metadataColnames
    )

    eventUI <- selectizeInput(
        inputId   = "fillInOverallEventDeath",
        label     = " ",
        choices   = metadataColnames
    )

    ui <- tagList(
        tags$div(class = "row",
            tags$div(class = "col-sm-4 h4", "Standard"),
            tags$div(class = "col-sm-4 h4", "Colname"),
            tags$div(class = "col-sm-4 h4", "Description")
        ),
        tags$hr(),
        tags$div(class = "row",
            tags$div(class = "col-sm-4 h4", "TimeOS"),
            tags$div(class = "col-sm-4 h5", timeUI),
            tags$div(class = "col-sm-4 h5", "Overall Survival Time")
        ),
        tags$div(class = "row",
            tags$div(class = "col-sm-4 h4", "EventDeath"),
            tags$div(class = "col-sm-4 h5", eventUI),
            tags$div(class = "col-sm-4 h5", "Flag for Death or alive (censor) (Death = 1)")
        )
    )

    return(ui)
}
