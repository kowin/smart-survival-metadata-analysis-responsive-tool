
realTimeUnit <- function(input, type) {
    timeUnit <- input[[paste0(type, "TimeUnit")]]
    intervalType <- input[[paste0(type, "IntervalType")]]

    if (is.null(timeUnit) | is.null(intervalType)) {
        return(NULL)
    }

    realUnit <- switch(timeUnit,
        "day" = .dayUnits(intervalType),
        "week" = .weekUnits(intervalType),
        "month" = .monthUnits(intervalType),
        "year" = .yearUnits(intervalType),
       " NULL"
    )

    return(realUnit)
}

.dayUnits <- function(intervalType) {
    unit <- switch(intervalType,
        "1" = "Day",
        "/7" = "Week",
        "/365 *12" = "Month",
        "/365" = "Year",
        NULL
    )
    return(unit)
}

.weekUnits <- function(intervalType) {
    unit <- switch(intervalType,
        "*7" = "Day",
        "1" = "Week",
        "*7 /365 *12" = "Month",
        "*7 /365" = "Year",
        NULL
    )
    return(unit)
}

.monthUnits <- function(intervalType) {
    unit <- switch(intervalType,
        "/12 *365" = "Day",
        "/12 *365 /7" = "Week",
        "1" = "Month",
        "/12" = "Year",
        NULL
    )
    return(unit)
}

.yearUnits <- function(intervalType) {
    unit <- switch(intervalType,
        "*365" = "Day",
        "*365 /7" = "Week",
        "*12" = "Month",
        "1" = "Year",
        NULL
    )
    return(unit)
}
