
twoDigits <- function(str) {
    return(round(as.numeric(str), 2))
}
