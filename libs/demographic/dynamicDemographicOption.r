
dynamicDemographicOption <- function(variable, level, type) {
    if (is.null(variable) |
        is.null(level) |
        !is.null(variable["Error", "Error Message"])
    ) {
        return(NULL)
    }

    categorical <- variable[variable$Type %in% c("Categorical", "Nominal", "Ordinal"),]
    choices <- setNames(categorical$Colname, categorical$Label)

    levelCount <- (summary(factor(level$Colname)))
    twoClass <- names(levelCount[levelCount == 2])
    otherClass <- names(levelCount[levelCount > 2])

    overallClassChoices <- c("Overall" = "OverallClassChoices")
    standardClassChoices <- c("EventDeath" = "EventDeath")
    twoClassChoices <- choices[choices %in% twoClass]
    otherClassChoices <- choices[choices %in% otherClass]

    ui <- selectizeInput(
        inputId   = paste0(type, "DemographicOption"),
        label     = "Stratification",
        width     = "100%",
        selected  = "EventDeath",
        choices   = list(
            "One level column" = overallClassChoices,
            "Standard column" = standardClassChoices,
            "Two level column" = twoClassChoices,
            "Three or more level column" = otherClassChoices
        )
    )

    return(ui)
}
