
continuousDemographic <- function(input, dataset, variable, level, type) {
    splitterOption <- input[[paste0(type, "DemographicOption")]]
    if (is.null(splitterOption) | is.null(dataset)) {
        return(NULL)
    }

    continuousColname <- variable$Colname[variable$Type %in% c("Continuous", "Interval", "Ratio")]
    continuousLabel <- variable$Label[variable$Type %in% c("Continuous", "Interval", "Ratio")]

    if (0 == length(continuousColname)) {
        return(NULL)
    }

    splitterLevel <- dplyr::filter(level, Colname == splitterOption)
    splitter <- splitterLevel$Label

    pValueList <- list()
    if (length(splitter) > 2) {
        withProgress(
            message = 'Calculate ANOVA.',
            detail = "Count size of continuous column.",
            value = 0,
            expr = {
                continuousColnameLength <- length(continuousColname)
                unitSize <- 1 / continuousColnameLength
                for (r in 1:continuousColnameLength) {
                    colname <- continuousColname[r]
                    label <- continuousLabel[r]
                    incProgress(
                        amount = unitSize,
                        detail = paste("Calculate ANOVA value of ", label)
                    )
                    ## using anova
                    anovaFormula <- paste0("aov(", colname, " ~ ", splitterOption, ", data = dataset)")
                    anovaResult <- eval(parse(text = anovaFormula))
                    anovaSummary <- summary(anovaResult)
                    pValue <- anovaSummary[[1]][["Pr(>F)"]][[1]]
                    pValueList[[label]] <- pValue
                }
            }
        )
    }

    if ("OverallClassChoices" == splitterOption) {
        table <- .overallClassContinuousDemographic(dataset, continuousColname, continuousLabel)
    } else {
        dataList <- dataListAndTitle(dataset, splitter, splitterOption, continuousColname, continuousLabel)
        table <- .moreClassContinuousDemographic(dataList, pValueList)
    }
    return(table)
}

.overallClassContinuousDemographic <- function(dataset, continuousColname, continuousLabel) {
    subData <- dataset[, continuousColname]
    # fix the bug of only one continuous column in Overall mode
    subData <- as.data.frame(subData)
    colnames(subData) <- continuousLabel
    datasetList <- list()
    datasetList[[1]] <- subData
    tableTitle <- paste0("All data (n = ", count(subData)[[1]], ")")
    continuousDemographicTable <- data.frame()
    continuousDemographicTable[c("Variable", tableTitle)] <- NULL

    withProgress(
        message = 'Calculate continuous demographic value.',
        detail = "Count size of continuous column.",
        value = 0,
        expr = {
            continuousColname <- colnames(datasetList[[1]])
            continuousColnameLength <- length(continuousColname)
            unitSize <- 1 / continuousColnameLength
            for (columnName in continuousColname) {
                incProgress(
                    amount = unitSize,
                    detail = paste0("Calculate row value of ", columnName)
                )
                isNomalDistribution <- .isNomalDistribution(datasetList, columnName)
                if ("no data" == isNomalDistribution) {
                    next
                }

                continuousRow <- .calculateContinuousRow(datasetList, columnName, isNomalDistribution)

                continuousDemographicTable["Variable", columnName] <- columnName
                continuousDemographicTable[tableTitle, columnName] <- continuousRow
            }
        }
    )
    return(t(continuousDemographicTable))
}

.moreClassContinuousDemographic <- function(dataList, pValueList) {
    datasetList <- dataList[[1]]
    tableTitle <- dataList[[2]]

    continuousDemographicTable <- data.frame()
    continuousDemographicTable[c("Variable", tableTitle, "P value"),] <- NULL

    withProgress(
        message = 'Calculate continuous demographic value.',
        detail = "Count size of continuous column.",
        value = 0,
        expr = {
            continuousColname <- colnames(datasetList[[1]])
            continuousColnameLength <- length(continuousColname)
            unitSize <- 1 / continuousColnameLength
            for (columnName in continuousColname) {
                incProgress(
                    amount = unitSize,
                    detail = paste0("Calculate row value of ", columnName)
                )
                isNomalDistribution <- .isNomalDistribution(datasetList, columnName)
                if ("no data" == isNomalDistribution) {
                    next
                }

                continuousRow <- .calculateContinuousRow(datasetList, columnName, isNomalDistribution)

                pValue <- .continuousPvalue(isNomalDistribution, datasetList, columnName, pValueList)
                continuousDemographicTable["Variable", columnName] <- columnName
                continuousDemographicTable[tableTitle, columnName] <- continuousRow
                continuousDemographicTable["P value", columnName] <- pValue
            }
        }
    )
    return(t(continuousDemographicTable))
}

.calculateContinuousRow <- function(datasetList, columnName, isNomalDistribution) {
    continuousRow <- c()
    withProgress(
        message = 'Calculate continuous demographic value.',
        detail = "Count size of continuous column.",
        value = 0,
        expr = {
            datasetListLength <- length(datasetList)
            unitSize <- 1 / datasetListLength
            for (r in 1:length(datasetList)) {
                incProgress(
                    amount = unitSize,
                    detail = paste0("Calculate cell value of ", columnName, " [", r, "].")
                )
                subData <- datasetList[[r]][,columnName]
                if (isNomalDistribution) {
                    cell <- .describeParametricCell(subData)
                } else {
                    cell <- .describeNonParametricCell(subData)
                }
                continuousRow <- c(continuousRow, cell)
            }
        }
    )
    return(continuousRow)
}

.isNomalDistribution <- function(datasetList, columnName) {
    allData <- c()
    for (singleDataset in datasetList) {
        subData <- singleDataset[,columnName]
        allData <- c(allData, subData)
    }

    dataCount <- sum(!is.na(allData))
    if (dataCount < 3) {
        result <- "no data"
        return(result)
    }

    ## normal distribution test
    ### reference paper
    ### Razali, Nornadiah Mohd, and Yap Bee Wah. "Power comparisons of shapiro-wilk, kolmogorov-smirnov, lilliefors and anderson-darling tests." Journal of statistical modeling and analytics 2.1 (2011): 21-33.
    if (dataCount < 5000) {
        ### Shapiro-Wilk Normality Test
        shapiroTest <- shapiro.test(allData)
        isNomalDistribution <- (shapiroTest$p.value > 0.05)
    } else {
        ### Anderson-Darling normality test
        adTest <- ad.test(allData)
        isNomalDistribution <- (adTest$p.value > 0.05)
    }
    return(isNomalDistribution)
}

.continuousPvalue <- function(isNomalDistribution, datasetList, columnName, pValueList) {
    pValue <- "no data"

    if (2 == length(datasetList)) {
        leftData <- datasetList[[1]][,columnName]
        rightData <- datasetList[[2]][,columnName]
        if (.isNeedTestPvalue(leftData, rightData)) {
            if (isNomalDistribution) {
                ## parametric statics
                tTest <- t.test(leftData, rightData)
                pValue <- tTest$p.value
            } else {
                ## nonparametric statics
                wilcoxTest <- wilcox.test(leftData, rightData, exact = FALSE)
                pValue <- wilcoxTest$p.value
            }
        }
    } else {
        pValue <- pValueList[[columnName]]
    }

    pValue <- showPvalue(pValue)
    return(pValue)
}

.describeParametricCell <- function(dataset) {
    if (sum(!is.na(dataset)) < 3) {
        return("no data")
    }

    dataMean <- mean(dataset, na.rm = TRUE)
    dataSD <- sd(dataset, na.rm = TRUE)
    result <- paste0(twoDigits(dataMean), " ± ", twoDigits(dataSD))
    return(result)
}

.describeNonParametricCell <- function(dataset) {
    if (sum(!is.na(dataset)) < 3) {
        return("no data")
    }
    dataMean <- twoDigits(median(dataset, na.rm = TRUE))
    dataMin <- twoDigits(min(dataset, na.rm = TRUE))
    dataMax <- twoDigits(max(dataset, na.rm = TRUE))
    result <- paste0(dataMean, "(", dataMin, "-", dataMax, ")")
    return(result)
}

.isNeedTestPvalue <- function(leftData, rightData) {
    hasLeftData <- !(sum(!is.na(leftData)) < 3)
    hasRightData <- !(sum(!is.na(rightData)) < 3)
    return(hasLeftData & hasRightData)
}
