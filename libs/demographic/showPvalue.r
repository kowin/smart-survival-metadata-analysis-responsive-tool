
showPvalue <- function(pValueList) {
    threeDigits <- round(as.numeric(pValueList), 3)
    newPvalue <- ifelse((threeDigits < 0.001), "< 0.001", threeDigits)

    # add star on the p-value <= 0.05
    isAddStar <- (newPvalue <= 0.05 & newPvalue != "no data")
    newPvalue <- ifelse(isAddStar, paste0(newPvalue, " *"), newPvalue)
    return(newPvalue)
}
