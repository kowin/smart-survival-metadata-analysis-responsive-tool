
categoricalDemographic <- function(input, dataset, variable, level, type) {
    splitterOption <- input[[paste0(type, "DemographicOption")]]
    if (is.null(splitterOption) | is.null(dataset)) {
        return(NULL)
    }

    categoricalColname <- variable$Colname[variable$Type %in% c("Categorical", "Nominal", "Ordinal")]
    categoricalLabel <- variable$Label[variable$Type %in% c("Categorical", "Nominal", "Ordinal")]

    if (0 == length(categoricalColname)) {
        return(NULL)
    }

    if ("OverallClassChoices" == splitterOption) {
        table <- .overallClassCategoricalDemographic(dataset, categoricalColname, categoricalLabel)
    } else {
        splitterLevel <- dplyr::filter(level, Colname == splitterOption)
        splitter <- splitterLevel$Label

        dataList <- dataListAndTitle(dataset, splitter, splitterOption, categoricalColname, categoricalLabel)

        table <- .moreClassCategoricalDemographic(dataList)
    }
    return(table)
}

.overallClassCategoricalDemographic <- function(dataset, categoricalColname, categoricalLabel) {
    subData <- dataset[, categoricalColname]
    colnames(subData) <- categoricalLabel
    datasetList <- list()
    datasetList[[1]] <- subData
    tableTitle <- paste0("All data (n = ", count(subData)[[1]], ")")
    categoricalDemographicTable <- data.frame()
    categoricalDemographicTable[c("Variable", tableTitle)] <- NULL

    withProgress(
        message = 'Calculate categorical demographic value.',
        detail = "Count size of categorical column.",
        value = 0,
        expr = {
            categoricalLabelLength <- length(categoricalLabel)
            unitSize <- 0.5 / categoricalLabelLength
            for (columnName in categoricalLabel) {
                incProgress(
                    amount = unitSize,
                    detail = paste0("Calculate row value of ", columnName)
                )

                categoricalTable <- .describeCategoricalTable(datasetList, columnName)
                tableData <- categoricalTable[[1]]
                levelData <- categoricalTable[[2]]

                if (is.null(tableData)) {
                    incProgress(amount = unitSize)
                    next
                }

                categoricalDemographicTable["Variable", columnName] <- columnName
                categoricalDemographicTable[tableTitle, columnName] <- ""

                oneTable <- .oneVariableTabulate(tableData)

                incProgress(
                    amount = unitSize,
                    detail = paste0("Put the tabulate value of ", columnName, " into demographic.")
                )
                subResult <- c()
                for (r in 1:length(levelData)) {
                    levelCell <- paste0(columnName, " - ", levelData[r])
                    categoricalDemographicTable["Variable", levelCell] <- levelCell
                    categoricalDemographicTable[tableTitle, levelCell] <- oneTable[r,]
                }
            }
        }
    )
    return(t(categoricalDemographicTable))
}

.moreClassCategoricalDemographic <- function(dataList) {
    datasetList <- dataList[[1]]
    tableTitle <- dataList[[2]]
    categoricalDemographicTable <- data.frame()
    categoricalDemographicTable[c("Variable", tableTitle, "P value"),] <- NULL

    withProgress(
        message = 'Calculate categorical demographic value.',
        detail = "Count size of categorical column.",
        value = 0,
        expr = {
            categoricalLabel <- colnames(datasetList[[1]])
            categoricalLabelLength <- length(categoricalLabel)
            unitSize <- 0.25 / categoricalLabelLength
            for (columnName in categoricalLabel) {
                incProgress(
                    amount = unitSize,
                    detail = paste0("Calculate row value of ", columnName)
                )
                categoricalTable <- .describeCategoricalTable(datasetList, columnName)
                tableData <- categoricalTable[[1]]
                levelData <- categoricalTable[[2]]

                if (is.null(tableData)) {
                    incProgress(amount = unitSize * 3)
                    next
                }

                incProgress(
                    amount = unitSize,
                    detail = paste0("Calculate p-value of ", columnName)
                )
                pValue <- .categoricalPvalue(tableData)

                categoricalDemographicTable["Variable", columnName] <- columnName
                categoricalDemographicTable[tableTitle, columnName] <- ""
                categoricalDemographicTable["P value", columnName] <- pValue

                incProgress(
                    amount = unitSize,
                    detail = paste0("Calculate tabulate value of ", columnName)
                )
                oneTable <- .oneVariableTabulate(tableData)

                incProgress(
                    amount = unitSize,
                    detail = paste0("Put the tabulate value of ", columnName, " into demographic.")
                )
                subResult <- c()
                for (r in 1:length(levelData)) {
                    levelCell <- paste0(columnName, " - ", levelData[r])
                    categoricalDemographicTable["Variable", levelCell] <- levelCell
                    categoricalDemographicTable[tableTitle, levelCell] <- oneTable[r,]
                    categoricalDemographicTable["P value", levelCell] <- ""
                }
            }
        }
    )
    return(t(categoricalDemographicTable))
}

.oneVariableTabulate <- function(tableData) {
    propTableData <- prop.table(tableData, 2) * 100
    oneVariableTableData <- abind(tableData, propTableData, along = 3)
    oneTable <- apply(
        X = oneVariableTableData,
        MARGIN = c(1,2),
        FUN = function(x) {
            paste0(x[1], " (", twoDigits(x[2]), "%)")
        }
    )
    return(oneTable)
}

.categoricalPvalue <- function(tableData) {
    pValue <- NULL
    chisqTest <- chisq.test(tableData)
    pValue <- showPvalue(chisqTest$p.value)
    return(pValue)
}

.describeCategoricalTable <- function(datasetList, columnName) {
    subResult <- c()
    nonZeroLevel <- c()

    withProgress(
        message = 'Calculate categorical demographic table value.',
        detail = "Count size of categorical level column.",
        value = 0,
        expr = {
            levelLength <- length(levels(datasetList[[1]][,columnName]))
            unitSize <- 1 / levelLength
            for (level in levels(datasetList[[1]][,columnName])) {
                incProgress(
                    amount = unitSize,
                    detail = paste0("Calculate value of ", columnName, " on the ", level, " level.")
                )
                hasNA <- FALSE
                isAllZero <- TRUE
                countList <- c()
                for (i in 1 : length(datasetList)) {
                    subCount <- sum(datasetList[[i]][,columnName] == level, na.rm = TRUE)
                    if (is.na(subCount)) {
                        hasNA <- TRUE
                        next
                    }
                    if (0 != subCount) {
                        isAllZero <- FALSE
                    }
                    countList <- c(countList, subCount)
                }
                # chisq does not allow NA
                if (hasNA) {
                    next
                }
                # it is no meaning if both of value on this class are zero
                if (isAllZero) {
                    next
                }
                nonZeroLevel <- c(nonZeroLevel, level)
                subResult <- c(subResult, countList)
            }
        }
    )

    resultDataTable <- NULL
    if (!is.null(subResult)) {
        resultDataTable <- matrix(subResult, ncol = length(datasetList), byrow = TRUE)
        resultDataTable <- as.table(resultDataTable)
    }

    result <- list(resultDataTable, nonZeroLevel)
    return(result)
}
