
dataListAndTitle <- function(dataset, splitter, splitterOption, colname, label) {
    datasetList <- list()
    tableTitle <- c()
    withProgress(
        message = 'Convert data list and title.',
        detail = "Count size of splitter.",
        value = 0,
        expr = {
            splitterLength <- length(splitter)
            unitSize <- 1 / splitterLength
            for (r in 1:splitterLength) {
                incProgress(
                    amount = unitSize,
                    detail = paste("Convert title of ", splitter[r], ".")
                )
                subData <- dataset[dataset[splitterOption] == splitter[r], colname]
                subData <- as.data.frame(subData)
                colnames(subData) <- label
                datasetList[[r]] <- subData
                subTitle <- paste0(splitter[r], " (n = ", count(subData)[[1]], ")")
                tableTitle <- c(tableTitle, subTitle)
            }
        }
    )
    result <- list(datasetList, tableTitle)
    return(result)
}
