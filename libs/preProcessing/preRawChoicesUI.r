
preRawChoicesUI <- function (input, session, choices) {
    updateSelectizeInput(
        session,
        inputId   = "preStartDateColumn",
        label     = "Start date column",
        choices   = choices
    )

    updateSelectizeInput(
        session,
        inputId   = "preEndDateColumn",
        label     = "End date column",
        choices   = choices
    )

    updateSelectizeInput(
        session,
        inputId   = "preEventReferenceColumn",
        label     = "Reference",
        choices   = choices
    )

    updateSelectizeInput(
        session,
        inputId   = "preColumnSelector",
        label     = paste0(
            "Select the column tag that you want to analysis. ",
            "(Use delete key or backspace key to remove tag.)"
        ),
        selected  = choices,
        choices   = choices
    )
}
