
transferCategorical <- function (dataset, variable, level) {
    if (is.null(dataset) |
        is.null(variable) |
        is.null(level) |
        !is.null(dataset["Error", "Error Message"]) |
        !is.null(variable["Error", "Error Message"])
    ) {
        return(NULL)
    }

    nominal <- dplyr::filter(variable, Type %in% c("Categorical", "Nominal"))
    dataset <- .transferNominal2Factor(dataset, nominal, level)

    ordinal <- dplyr::filter(variable, Type %in% c("Ordinal"))
    dataset <- .transferOrdinal2Ordered(dataset, ordinal, level)

    return(dataset)
}

.transferNominal2Factor <- function(dataset, nominal, level) {
    for (colname in nominal$Colname) {
        itemLabel <- dplyr::filter(level, Colname == colname)
        dataset[,colname] <- factor(
            dataset[,colname],
            levels = itemLabel$Value,
            labels = itemLabel$Label
        )
        dataset[,colname] <- factor(dataset[,colname])
    }
    return(dataset)
}

.transferOrdinal2Ordered <- function(dataset, ordinal, level) {
    for (colname in ordinal$Colname) {
        itemLabel <- dplyr::filter(level, Colname == colname)
        dataset[,colname] <- ordered(
            dataset[,colname],
            levels = itemLabel$Value,
            labels = itemLabel$Label
        )
        dataset[,colname] <- ordered(dataset[,colname])
    }

    return(dataset)
}
