
preProcessedChoicesUI <- function (input, session, choices, newColumn) {
    updateSelectizeInput(
        session,
        inputId   = "preColumnSelector",
        label     = paste0(
            "Select the column tag that you want to analysis. ",
            "(Use delete key or backspace key to remove tag.)"
        ),
        selected  = c(input$preColumnSelector, newColumn),
        choices   = choices
    )
}
