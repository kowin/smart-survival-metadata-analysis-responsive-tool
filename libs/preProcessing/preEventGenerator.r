
preEventGenerator <- function (input, dataset) {
    tryCatch({
        formula <- paste0(
            "dataset$", input$preEventColumnName, " <- ",
            "ifelse(dataset$", input$preEventReferenceColumn,
            input$preEventReferenceCompare, input$preEventReferenceValue,
            ", 1, 0)"
        )
        eval(parse(text = formula))
    }, warning = function(w) {
    }, error = function(e) {
    }, finally = {
    })

    return(dataset)
}
