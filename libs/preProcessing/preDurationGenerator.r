
preDurationGenerator <- function (input, dataset) {
    tryCatch({
        formula <- paste0(
            "dataset$", input$preStartDateColumn, " <- ",
            "as.Date(dataset$", input$preStartDateColumn, ")"
        )
        eval(parse(text = formula))
        formula <- paste0(
            "dataset$", input$preEndDateColumn, " <- ",
            "as.Date(dataset$", input$preEndDateColumn, ")"
        )
        eval(parse(text = formula))
    }, warning = function(w) {
    }, error = function(e) {
    }, finally = {
        tryCatch({
            formula <- paste0(
                "dplyr::mutate(dataset, ",
                input$preDurationColumnName, " = ",
                input$preEndDateColumn, " - ", input$preStartDateColumn, ")"
            )
            dataset <- eval(parse(text = formula))
        }, warning = function(w) {
        }, error = function(e) {
        }, finally = {
        })
    })

    return(dataset)
}
