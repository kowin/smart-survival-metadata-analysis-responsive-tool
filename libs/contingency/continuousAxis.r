
continuousAxis <- function(variable, showStandard = FALSE) {
    if (!is.null(variable)) {
        variable <- dplyr::filter(variable, Type %in% c("Continuous", "Interval", "Ratio"))
        axis <- setNames(variable$Colname, variable$Label)
        if (showStandard) {
            axis <- list(
                "Standard Column" = c("TimeOS" = "TimeOS"),
                "Dataset Column" = axis
            )
        }
    } else {
        axis <- c(" ")
    }

    return(axis)
}
