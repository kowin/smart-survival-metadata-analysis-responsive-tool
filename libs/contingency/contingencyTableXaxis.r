
contingencyTableXaxis <- function (axis, type) {
    ui <- selectInput(
        inputId   = paste0(type, "ContingencyTableXaxis"),
        label     = tags$h6("Column label"),
        width     = "200px",
        choices   = axis
    )
    return(ui)
}
