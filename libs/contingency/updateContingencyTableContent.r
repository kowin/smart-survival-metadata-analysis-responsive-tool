
updateContingencyTableContent <- function (dataset, input, type) {
    options <- input[[paste0(type, "ContingencyTableOption")]]
    axisX <- input[[paste0(type, "ContingencyTableXaxis")]]
    axisY <- input[[paste0(type, "ContingencyTableYaxis")]]
    sumPercentType <- input[[paste0(type, "SumPercentType")]]

    if (is.null(dataset) |
        is.null(axisX) |
        is.null(axisY) |
        isTRUE(" " == axisX) |
        isTRUE(" " == axisY) |
        is.null(options)
    ) {
        return(NULL)
    }

    countFormula <- paste0("with(dataset, table(", axisY, ", ", axisX, "))")
    countTabulate <- eval(parse(text = countFormula))

    if ("row" == sumPercentType) {
        countTabulate <- addmargins(countTabulate, margin = 1)
        percentTabulate <- round(prop.table(countTabulate, 1) * 100, 1)
        countTabulate <- addmargins(countTabulate, margin = 2)
        percentTabulate <- addmargins(percentTabulate, margin = 2)
    } else if ("column" == sumPercentType) {
        countTabulate <- addmargins(countTabulate, margin = 2)
        percentTabulate <- round(prop.table(countTabulate, 2) * 100, 1)
        countTabulate <- addmargins(countTabulate, margin = 1)
        percentTabulate <- addmargins(percentTabulate, margin = 1)
    } else {
        percentTabulate <- prop.table(countTabulate)
        percentTabulate <- addmargins(percentTabulate)
        percentTabulate <- round(percentTabulate * 100, 1)
        countTabulate <- addmargins(countTabulate)
    }

    tabulate <- NULL
    if (("count" %in% options) & ("percent" %in% options)) {
        allDataTabulate <- abind(countTabulate, percentTabulate, along = 3)
        allTabulate <- apply(
            X = allDataTabulate,
            MARGIN = c(1,2),
            FUN = function(x) {
                paste0(x[1], " (", x[2], "%)")
            }
        )
        tabulate <- allTabulate
    } else if (("count" %in% options) & !("percent" %in% options)) {
        countTabulate <- as.data.frame.matrix(countTabulate)
        tabulate <- countTabulate
    } else if (!("count" %in% options) & ("percent" %in% options)) {
        percentTabulate <- as.data.frame.matrix(percentTabulate)
        tabulate <- apply(
            X = percentTabulate,
            MARGIN = c(1,2),
            FUN = function(x) {
                paste0(x[1], "%")
            }
        )
    }

    return(tabulate)
}
