
contingencyTableOption <- function (type) {
    ui <- checkboxGroupInput(
        inputId   = paste0(type, "ContingencyTableOption"),
        label     = "Show type",
        selected  = c("count", "percent"),
        choices   = c(
            "No. of patient" = "count",
            "% of patient" = "percent"
        )
    )
    return(ui)
}
