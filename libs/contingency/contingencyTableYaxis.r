
contingencyTableYaxis <- function (axis, type) {
    yaxis <- selectInput(
        inputId   = paste0(type, "ContingencyTableYaxis"),
        label     = tags$h6("Row label"),
        width     = "200px",
        choices   = axis
    )

    sumPercentType <- buttonTypeRadioButton(
        inputId   = paste0(type, "SumPercentType"),
        label     = tags$h6("Sum percent by"),
        inline    = TRUE,
        selected  = "column",
        choices   = c(
            "Row" = "row",
            "Column" = "column",
            "Overall" = "overall"
        ),
        specialClass = " btn-group"
    )

    ui <- tagList(yaxis, sumPercentType)
    return(ui)
}
