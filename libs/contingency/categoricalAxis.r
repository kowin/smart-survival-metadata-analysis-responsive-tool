
categoricalAxis <- function(variable, showStandard = FALSE, showOverall = FALSE) {
    if (!is.null(variable)) {
        variable <- dplyr::filter(variable, Type %in% c("Categorical", "Nominal", "Ordinal"))
        axis <- setNames(variable$Colname, variable$Label)

        if (showOverall) {
            overallClassChoices <- c("Overall" = "OverallClassChoices")
        } else {
            overallClassChoices <- NULL
        }

        if (showStandard) {
            standardClassChoices <- c("EventDeath" = "EventDeath")
        } else {
            standardClassChoices <- NULL
        }

        if (showOverall | showStandard) {
            axis <- list(
                "Overall" = overallClassChoices,
                "Standard Column" = standardClassChoices,
                "Dataset Column" = axis
            )
        }
    } else {
        axis <- c(" ")
    }

    return(axis)
}
