
dynamicShowPartOption <- function(type) {
    showPart <- selectizeInput(
        inputId   = paste0(type, "ShowPartCheckbox"),
        label     = tags$h5("Please choose what you want to see."),
        multiple  = TRUE,
        selected  = showSelectedChoices(type),
        choices   = list(
            "Survival" = showSurvivalPart(type),
            "Descriptive Statistics" = showDescriptiveStatistics(type),
            "Metadata Browser" = showMetadataBrowser(type)
        ),
        options   = list(
        )
    )

    return(showPart)
}
