
showMetadataBrowser <- function(type) {
    choices <- c(
        "Standard Column Browser (Layer 1 Metadata)" = paste0(type, "OverallMetadataFieldset"),
        "Variable Manager Browser (Layer 2 Metadata)" = paste0(type, "VariableMetadataFieldset"),
        "Categorical Reference Browser (Layer 3 Metadata)" = paste0(type, "LevelMetadataFieldset")
    )
    return(choices)
}
