
showSurvivalPart <- function(type) {
    choices <- c(
        "Survival Formula" = paste0(type, "SurvivalFormula"),
        "Survival Formula Options" = paste0(type, "SurvivalFormulaOptionFieldset"),
        "Survival Curve Options" = paste0(type, "SurvivalCurveOptionFieldset"),
        "Survival Curve" = paste0(type, "SurvivalCurveFieldset")
    )
    return(choices)
}
