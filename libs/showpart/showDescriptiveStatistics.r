
showDescriptiveStatistics <- function(type) {
    choices <- c(
        "Survival Demographic Options" = paste0(type, "DemographicOptionFieldset"),
        "Categorical Demographic" = paste0(type, "CategoricalDemographicFieldset"),
        "Continuous Demographic" = paste0(type, "ContinuousDemographicFieldset"),
        "Pivot Table (Contingency Table) (Cross tabulation)" = paste0(type, "ContingencyTableFieldset"),
        "Clean Data" = paste0(type, "CleanDataFieldset")
    )
    return(choices)
}
