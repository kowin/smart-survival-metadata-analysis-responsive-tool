
showPartChoices <- function(type) {
    choices <- c(
        showSurvivalPart(type),
        showDescriptiveStatistics(type),
        showMetadataBrowser(type)
    )
    return(choices)
}
