
showSelectedChoices <- function(type) {
    selected <- switch(type,
        embedded = .showEmbeddedSelectedChoices(),
        upload   = .showUploadSelectedChoices(),
        NULL
    )
    return(selected)
}

.showEmbeddedSelectedChoices <- function() {
    selected <- c(
        "embeddedSurvivalFormula",
        "embeddedVariableMetadataFieldset"
    )
    return(selected)
}

.showUploadSelectedChoices <- function() {
    selected <- c(
        "uploadCleanDataFieldset",
        "uploadOverallMetadataFieldset",
        "uploadVariableMetadataFieldset",
        "uploadLevelMetadataFieldset"
    )
    return(selected)
}
