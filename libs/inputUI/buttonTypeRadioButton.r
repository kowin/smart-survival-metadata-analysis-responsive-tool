
buttonTypeRadioButton <- function(
    inputId,
    label,
    choices,
    selected = NULL,
    inline = TRUE,
    width = NULL,
    specialClass = NULL) {

    # resolve names
    choices <- choicesWithNames(choices)

    selected <- restoreInput(id = inputId, default = selected)

    # default value if it's not specified
    if (is.null(selected)) {
        selected <- choices[[1]]
    } else {
        selected <- validateSelected(selected, choices, inputId)
    }

    if (length(selected) > 1) {
        stop("The 'selected' argument must be of length 1")
    }

    options <- generateButtonOptions(inputId, choices, selected, inline, specialClass)

    divClass <- "form-group shiny-input-radiogroup shiny-input-container"

    style <- ""
    if (!is.null(width)) {
        paste0("width: ", validateCssUnit(width), ";")
    }

    ui <- tags$div(
        id = inputId,
        style = style,
        class = divClass,
        controlLabel(inputId, label),
        options
    )
    return(ui)
}
