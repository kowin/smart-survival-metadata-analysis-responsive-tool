
dataTableOption <- function(
    pageLength = 5,
    scrollY = 200,
    paging = TRUE,
    searching = TRUE,
    rightColumn = NULL
) {
    option <- list(
        pageLength = pageLength,
        lengthMenu = list(c(1, 2, 3, 5, 10, 25, 50, -1), c(1, 2, 3, 5, 10, 25, 50, "All")),
        pagingType = "full",
        paging = paging,
        searching = searching,
        aoColumnDefs = list(
            list(
                targets = rightColumn,
                class = "text-right"
            )
        ),
        scrollX = TRUE,
        scrollY = scrollY
    )
    return(option)
}
