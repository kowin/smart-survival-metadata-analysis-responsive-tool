
numericTypeList <- function(dataset) {
    numericList <- c()
    columns <- colnames(dataset)
    withProgress(
        message = 'Determine numeric type list.',
        detail = "Count size of column.",
        value = 0,
        expr = {
            columnsLength <- length(columns)
            unitSize <- 1 / columnsLength
            for (col in 1:length(columns)) {
                incProgress(
                    amount = unitSize,
                    detail = paste0("Determine of ", columns[col])
                )
                isNumericType <- is.numeric(unlist(dataset[columns[col]]))
                if (isNumericType) {
                    numericList <- c(numericList, col-1)
                }
            }
        }
    )
    return(numericList)
}
