
generateButtonOptions <- function(inputId, choices, selected, inline, specialClass) {
    # generate a list of <input type=? [checked] />
    options <- mapply(
        choices,
        names(choices),
        FUN = function(value, name) {
            inputTag <- tags$input(
                type = "radio",
                name = inputId,
                value = value
            )

            labelClass <- "btn btn-default"
            if (value %in% selected) {
                inputTag$attribs$checked <- "checked"
                labelClass <- paste0(labelClass, " active")
            }

            return(tags$label(inputTag, tags$span(name), class = labelClass))
        },
        SIMPLIFY = FALSE,
        USE.NAMES = FALSE
    )

    groupClass <- "shiny-options-group"
    if (is.null(specialClass)) {
        if (inline) {
            groupClass <- paste0(groupClass, " btn-group btn-group-justified")
        } else {
            groupClass <- paste0(groupClass, " btn-group-vertical")
        }
    } else {
        groupClass <- paste0(groupClass, specialClass)
    }

    group <- tags$div(
        class = groupClass,
        `data-toggle` = "buttons",
        options
    )
    return(group)
}
