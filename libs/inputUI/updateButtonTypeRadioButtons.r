
updateButtonTypeRadioButtons <- function(
    session,
    inputId,
    label = NULL,
    choices = NULL,
    selected = NULL,
    inline = FALSE) {
    # you must select at least one radio button
    if (is.null(selected) && !is.null(choices)) {
        selected <- choices[[1]]
    }
    .updateButtonInputOptions(session, inputId, label, choices, selected, inline)
}

.updateButtonInputOptions <- function(
    session,
    inputId,
    label = NULL,
    choices = NULL,
    selected = NULL,
    inline = FALSE,
    specialClass = NULL) {

    if (!is.null(choices)) {
        choices <- choicesWithNames(choices)
    }

    if (!is.null(selected)) {
        selected <- validateSelected(selected, choices, session$ns(inputId))
    }

    options <- if (!is.null(choices)) {
        format(tagList(
            generateButtonOptions(session$ns(inputId), choices, selected, inline, specialClass)
        ))
    }

    message <- .dropNulls(list(label = label, options = options, value = selected))

    session$sendInputMessage(inputId, message)
}


# Given a vector or list, drop all the NULL items in it
.dropNulls <- function(x) {
    return(x[!vapply(x, is.null, FUN.VALUE=logical(1))])
}
