
uploadDataImportUI <- function (shortUrl = NULL) {
    ui <- tagList()
    if (!is.null(shortUrl)) {
        ui <- tagList(
            tags$h4("This is your own dataset URL:"),
            tags$a(tags$span(class = "h4", shortUrl), href = shortUrl),
            tags$br(),
            tags$br()
        )
    }

    ui <- tagList(
        ui,
        tags$fieldset(id = "uploadCleanDataFieldset", class = "singleLine",
            tags$legend("Dataset", class = "singleLine"),
            dataTableOutput(outputId = "uploadDatasetBrowser")
        ),
        tags$hr(),
        tags$div(class = "col-lg-4", id = "uploadLayer1",
            tags$fieldset(id = "uploadOverallMetadataFieldset", class = "singleLine",
                tags$legend("Layer 1: Standard Column", class = "singleLine"),
                dataTableOutput(outputId = "uploadOverallMetadata")
            )
        ),
        tags$div(class = "col-lg-4", id = "uploadLayer2",
            tags$fieldset(id = "uploadVariableMetadataFieldset", class = "singleLine",
                tags$legend("Layer 2: Variable Manager", class = "singleLine"),
                dataTableOutput(outputId = "uploadVariableMetadata")
            )
        ),
        tags$div(class = "col-lg-4", id = "uploadLayer3",
            tags$fieldset(id = "uploadLevelMetadataFieldset", class = "singleLine",
                tags$legend("Layer 3: Categorical Reference", class = "singleLine"),
                dataTableOutput(outputId = "uploadLevelMetadata")
            )
        )
    )
    return(ui)
}
