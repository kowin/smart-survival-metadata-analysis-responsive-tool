
saveUploadDataset <- function (overall, variable, level, rawData) {
    uuid <- UUIDgenerate()
    uuid <- gsub("-", "", uuid)
    tryCatch({
        write.csv(overall, file = paste0("data/", uuid, "Layer1.csv"), row.names = FALSE)
        write.csv(variable, file = paste0("data/", uuid, "Layer2.csv"), row.names = FALSE)
        write.csv(level, file = paste0("data/", uuid, "Layer3.csv"), row.names = FALSE)
        write.csv(rawData, file = paste0("data/", uuid, "Dataset.csv"), row.names = FALSE)
    }, warning = function(w) {
        saveUploadCsvWarning <<- w
    }, error = function(e) {
        saveUploadCsvError <<- e
    }, finally = {
    })
    return(uuid)
}
