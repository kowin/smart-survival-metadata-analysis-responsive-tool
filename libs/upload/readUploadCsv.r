
readUploadCsv <- function (uploadFile, type, fromUUID = FALSE) {
    if (is.null(uploadFile)) {
        return(noFileTable(type))
    }

    path <- ifelse(fromUUID, uploadFile, uploadFile$datapath)

    tryCatch({
        table <- read.csv(path,
            encoding = "utf8",
            strip.white = TRUE,
            stringsAsFactors = FALSE
        )
    }, warning = function(w) {
        readUploadCsvWarning <<- w
    }, error = function(e) {
        readUploadCsvError <<- e
    }, finally = {
        result <- checkReadTable(table, type)
        return(result)
    })
}
