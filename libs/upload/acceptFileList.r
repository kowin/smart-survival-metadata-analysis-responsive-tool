
acceptFileList <- function () {
    list <- c(
        'text/csv',
        'text/comma-separated-values',
        'text/plain',
        'application/vnd.ms-excel',
        '.csv',
        '.xls',
        '.xlsx'
    )
    return(list)
}
