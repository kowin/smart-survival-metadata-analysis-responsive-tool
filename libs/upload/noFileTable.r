
noFileTable <- function(type) {
    table <- data.frame()
    table["Error","Error Message"] <- paste0("Please upload your ", type, ".")
    return(table)
}