
readUploadFile <- function (uploadFile, type, fromUUID = FALSE) {
    if (is.null(uploadFile)) {
        return(noFileTable(type))
    }

    if (fromUUID) {
        fileExtension <- "csv"
    } else {
        name <- uploadFile$name
        nameLength <- nchar(name)
        fileExtension <- substr(name, nameLength-2, nameLength)
    }

    if ("csv" == fileExtension) {
        result <- readUploadCsv(uploadFile, type, fromUUID)
    } else {
        result <- readUploadExcel(uploadFile, type)
    }

    return(result)
}
