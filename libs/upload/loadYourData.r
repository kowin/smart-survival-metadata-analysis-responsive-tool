
loadYourData <- function(session, reactiveDataList, yourDataList) {
    incProgress(0, message = "Load your data into the SMART.")
    uploadAlert <- reactiveDataList[[1]]
    uploadMetadata <- reactiveDataList[[2]]
    uploadDataset <- reactiveDataList[[3]]
    rawData <- yourDataList[[1]]
    overall <- yourDataList[[2]]
    variable <- yourDataList[[3]]
    rawLevel <- yourDataList[[4]]
    uploadAlert$Message <- NULL
    uploadMetadata$Overall <- overall
    incProgress(0.05, detail = "Load layer 1 metadata down, start load layer 2.")
    uploadMetadata$Variable <- variable
    incProgress(0.05, detail = "Load layer 2 metadata down, start load layer 3.")
    level <- appendOriginLevel(rawData, variable, rawLevel)
    uploadMetadata$Level <- level
    incProgress(0.1, detail = "Load layer 3 metadata down, start load raw dataset.")
    uploadDataset$Raw <- rawData
    uploadDataset$RightColumnRaw <- numericTypeList(rawData)
    incProgress(0.1, detail = "Load dataset down, start ETL process.")
    clean <- extractTransformLoad(uploadDataset$Raw, uploadMetadata)
    incProgress(0.1, detail = "ETL process down, start load clean dataset.")
    uploadDataset$RightColumnClean <- numericTypeList(clean)
    uploadDataset$Clean <- clean
    incProgress(0.1, detail = "Load clean dataset down.")
    updateButtonTypeRadioButtons(
        session,
        inputId   = "dataImportShowSidebarPanel",
        label     = "Data import panel",
        inline    = TRUE,
        selected  = "Hide",
        choices   = c("Show", "Hide")
    )
    incProgress(0.1, detail = paste("Start hide sidebar."))
    result <- list(uploadAlert, uploadMetadata, uploadDataset)
    return(result)
}
