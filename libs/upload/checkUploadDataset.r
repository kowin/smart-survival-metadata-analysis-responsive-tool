
checkUploadDataset <- function (overall, variable, level, rawData) {
    result <- list(TRUE, c())

    if (!is.null(rawData["Error","Error Message"])) {
        result[[1]] <- FALSE
        result[[2]] <- c(result[[2]], rawData["Error","Error Message"])
    }

    if (!is.null(overall["Error","Error Message"])) {
        result[[1]] <- FALSE
        result[[2]] <- c(result[[2]], overall["Error","Error Message"])
    }

    if (!is.null(overall["Error","Need Column"])) {
        result[[1]] <- FALSE
        result[[2]] <- c(result[[2]], overall["Error","Need Column"])
    }

    if (!is.null(variable["Error","Error Message"])) {
        result[[1]] <- FALSE
        result[[2]] <- c(result[[2]], variable["Error","Error Message"])
    }

    if (!is.null(variable["Error","Need Column"])) {
        result[[1]] <- FALSE
        result[[2]] <- c(result[[2]], variable["Error","Need Column"])
    }

    if (!is.null(level["Error","Error Message"])) {
        result[[1]] <- FALSE
        result[[2]] <- c(result[[2]], level["Error","Error Message"])
    }

    if (!is.null(level["Error","Need Column"])) {
        result[[1]] <- FALSE
        result[[2]] <- c(result[[2]], level["Error","Need Column"])
    }

    return(result)
}
