
extractTransformLoad <- function (dataset, metadata) {
    overall <- metadata$Overall
    variable <- metadata$Variable
    level <- metadata$Level

    if (is.null(dataset) |
        is.null(overall) |
        is.null(variable) |
        is.null(level) |
        !is.null(dataset["Error", "Error Message"]) |
        !is.null(overall["Error", "Error Message"]) |
        !is.null(variable["Error", "Error Message"]) |
        !is.null(level["Error", "Error Message"])
    ) {
        return(NULL)
    }

    withProgress(
        message = 'Extract-Load-Transform process.',
        detail = "Replace categorical or nominal label by layer 2 metadata.",
        value = 0,
        expr = {
            # Replace categorical or nominal label by layer 2 metadata
            nominal <- dplyr::filter(variable, Type %in% c("Categorical", "Nominal"))
            dataset <- .transferNominal2Factor(dataset, nominal, level)

            incProgress(0.2, detail = "Replace ordinal label by layer 2 metadata.")
            ordinal <- dplyr::filter(variable, Type %in% c("Ordinal"))
            dataset <- .transferOrdinal2Ordered(dataset, ordinal, level)


            # Extend standard column by layer 0 metadata
            incProgress(0.2, detail = "Extend standard column by layer 0 metadata.")
            nameList <- c("TimeOS", "EventDeath", colnames(dataset))

            setting <- setNames(overall$Colname, overall$Standard)

            formula <- paste0("dplyr::mutate(dataset, TimeOS = ", setting["TimeOS"], ")")
            dataset <- eval(parse(text = formula))

            formula <- paste0("dplyr::mutate(dataset, EventDeath = ", setting["EventDeath"], ")")
            dataset <- eval(parse(text = formula))

            # Transfer categorical label in the standard column
            incProgress(0.2, detail = "Transfer categorical label in the standard column.")
            dataset[,"EventDeath"] <- factor(
                dataset[,"EventDeath"],
                levels = c(1,0),
                labels = c("Death", "Censored")
            )

            # re-order column
            incProgress(0.2, detail = "Re-order column.")
            dataset <- dataset[,nameList]
        }
    )
    return(dataset)
}

.transferNominal2Factor <- function(dataset, nominal, level) {
    withProgress(
        message = 'Transfer nominal column into user-define label.',
        detail = "Count size of nominal list.",
        value = 0,
        expr = {
            nominalSize <- count(nominal$Colname)[[1]]
            unitSize <- 1 / nominalSize
            for (colname in nominal$Colname) {
                incProgress(
                    amount = unitSize,
                    detail = paste("Convert ", colname, " column.")
                )
                itemLabel <- dplyr::filter(level, Colname == colname)
                dataset[,colname] <- factor(
                    dataset[,colname],
                    levels = itemLabel$Value,
                    labels = itemLabel$Label
                )
                dataset[,colname] <- factor(dataset[,colname])
            }
        }
    )
    return(dataset)
}

.transferOrdinal2Ordered <- function(dataset, ordinal, level) {
    withProgress(
        message = 'Transfer ordinal column into user-define label.',
        detail = "Count size of ordinal list.",
        value = 0,
        expr = {
            ordinalSize <- count(ordinal$Colname)[[1]]
            unitSize <- 1 / ordinalSize
            for (colname in ordinal$Colname) {
                incProgress(
                    amount = unitSize,
                    detail = paste("Convert ", colname, " column.")
                )
                itemLabel <- dplyr::filter(level, Colname == colname)
                dataset[,colname] <- ordered(
                    dataset[,colname],
                    levels = itemLabel$Value,
                    labels = itemLabel$Label
                )
                dataset[,colname] <- ordered(dataset[,colname])
            }
        }
    )
    return(dataset)
}
