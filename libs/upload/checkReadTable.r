
checkReadTable <- function (table, type) {
    check <- switch(type,
        "dataset"         = .checkRawData(),
        "layer1 metadata" = .checkLayer1Metadata(),
        "layer2 metadata" = .checkLayer2Metadata(),
        "layer3 metadata" = .checkLayer3Metadata(),
        NULL
    )

    if (!is.data.frame(table)) {
        return(.errorTable(type, check))
    }

    colname <- colnames(table)
    isOK <- .checkAllEqual(check, colname)

    if (!isOK) {
        return(.errorTable(type, check))
    }

    return(table)
}

.errorTable <- function(type, check) {
    table <- data.frame()
    table["Error","Error Message"] <- paste0("Please check your ", type, ".")
    table["Error","Need Column"] <- paste0("Need Column: ", paste(check, collapse = ", "))
    return(table)
}

.checkRawData <- function() {
    return(NULL)
}

.checkLayer1Metadata <- function() {
    check <- c("Standard", "Colname")
    return(check)
}

.checkLayer2Metadata <- function() {
    check <- c("Colname", "Type", "Label")
    return(check)
}

.checkLayer3Metadata <- function() {
    check <- c("Colname", "Value", "Label")
    return(check)
}

.checkAllEqual <- function(target, current) {
    if (is.null(target)) {
        return(TRUE)
    }
    return(isTRUE(all.equal(target, current)))
}
