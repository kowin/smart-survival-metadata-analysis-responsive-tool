
readUploadExcel <- function (uploadFile, type) {
    if (is.null(uploadFile)) {
        return(noFileTable(type))
    }

    tryCatch({
        path <- uploadFile$datapath
        name <- uploadFile$name
        nameLength <- nchar(name)
        fileExtension <- substr(name, nameLength-2, nameLength)
        if ("xls" == fileExtension) {
            table <- read_xls(path)
        } else {
            table <- read_xlsx(path)
        }
    }, warning = function(w) {
        readUploadExcelWarning <<- w
    }, error = function(e) {
        readUploadExcelError <<- e
    }, finally = {
        result <- checkReadTable(table, type)
        result <- as.data.frame(result)
        return(result)
    })
}
