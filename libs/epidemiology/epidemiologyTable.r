
epidemiologyTable <- function(dataset, level, input, type) {
    axis <- input[[paste0(type, "epidemiologyAxis")]]
    timeUnit <- input[[paste0(type, "RateTimeUnit")]]
    intervalType <- input[[paste0(type, "RateIntervalType")]]
    multiplePersonTime <- input[[paste0(type, "MultiplePersonTime")]]
    multiplePersonTime <- as.numeric(multiplePersonTime)

    if (is.null(axis) |
        is.null(timeUnit) |
        is.null(intervalType) |
        is.null(multiplePersonTime) |
        is.null(dataset) |
        is.null(level)
    ) {
        return(NULL)
    }

    commonSelector <- c("TimeOS", "EventDeath")

    if ("OverallClassChoices" == axis) {
        datasetList <- list(dataset[,commonSelector])
        tableTitle <- c(paste0("Overall (n = ", count(dataset)[[1]], ")"))
    } else {
        splitterLevel <- dplyr::filter(level, Colname == axis)
        splitter <- splitterLevel$Label

        dataList <- dataListAndTitle(dataset, splitter, axis, commonSelector, commonSelector)

        datasetList <- dataList[[1]]
        tableTitle <- dataList[[2]]
    }

    realUnit <- realTimeUnit(input, "uploadRate")
    rawTitle <- c()
    rawTitle[1] <- "Deaths (Mortality)"
    rawTitle[2] <- paste0("Mortality Incidence Density per ", sprintf("%9d", multiplePersonTime), " Person-", realUnit)
    rawTitle[3] <- paste0("Sum of Person-", realUnit)

    epidemiology <- data.frame()
    epidemiology[rawTitle,] <- NULL

    withProgress(
        message = 'Calculate epidemiology table.',
        detail = "Count size of dataset list.",
        value = 0,
        expr = {
            datasetListLength <- length(datasetList)
            unitSize <- 1 / datasetListLength
            for (c in 1:datasetListLength) {
                incProgress(
                    amount = unitSize,
                    detail = paste("Convert title of ", tableTitle[c], ".")
                )

                data <- datasetList[[c]]

                totalDeaths <- count(dplyr::filter(data, EventDeath == "Death"))[[1]]
                totalNewCases <- count(data)[[1]]
                totalTime <- .totalPersonTime(data, intervalType)

                deaths <- paste0(totalDeaths, " (", sprintf("%.2f", 100 * (totalDeaths / totalNewCases)), "%)")
                mortalityRate <- sprintf("%.2f", multiplePersonTime * (totalDeaths / totalTime))
                totalTimeString <- sprintf("%.2f", totalTime)

                epidemiology[,tableTitle[c]] <- c(deaths, mortalityRate, totalTimeString)
            }
        }
    )

    return(epidemiology)
}

.totalPersonTime <- function(data, intervalType) {
    totalTimeOrigin <- sum(data$TimeOS)
    totalTime <- totalTimeOrigin
    if (!is.null(intervalType)) {
        if ("1" != intervalType) {
            timeTransformer <- paste0(
                "totalTime <- totalTimeOrigin ",
                intervalType
            )
            eval(parse(text = timeTransformer))
        }
    }
    return(totalTime)
}
