
epidemiologyAxis <- function(axis, type) {
    ui <- selectInput(
        inputId   = paste0(type, "epidemiologyAxis"),
        label     = tags$h5("Stratification"),
        width     = "300px",
        choices   = axis
    )
    return(ui)
}
