# include all UI file
# easy to re-organiaze

source("libs/upload/acceptFileList.r", encoding = "UTF-8")
source("libs/inputUI/generateButtonOptions.r", encoding = "UTF-8")
source("libs/inputUI/buttonTypeRadioButton.r", encoding = "UTF-8")
source("libs/inputUI/input-utils.r", encoding = "UTF-8")

source("ui/logoList.r", encoding = "UTF-8")

source("ui/uploadSidebarPanel.r", encoding = "UTF-8")
source("ui/simpleTutorialPanel.r", encoding = "UTF-8")
source("ui/dataImportTabPanel.r", encoding = "UTF-8")
source("ui/dataFilterTabPanel.r", encoding = "UTF-8")
source("ui/descriptiveStatisticsTabPanel.r", encoding = "UTF-8")
source("ui/survivalAnalysisTabPanel.r", encoding = "UTF-8")

source("ui/variablesAppenderTabPanel.r", encoding = "UTF-8")
source("ui/metadataTabPanel.r", encoding = "UTF-8")

source("ui/embeddedSidebarPanel.r", encoding = "UTF-8")
source("ui/embeddedTabPanel.r", encoding = "UTF-8")

source("ui/tutorialTabPanel.r", encoding = "UTF-8")
