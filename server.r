library(shiny)
library(dplyr)

# assign port
options(shiny.port = 3838)
# assign max upload size equals 64 MB
options(shiny.maxRequestSize = 64 * 1024 * 1024)

source("includeAll.r", encoding = "UTF-8")


server <- function(input, output, session) {
    # upload panel
    ## the values between each panel
    uploadMetadata <- reactiveValues(
        Overall = NULL, # Layer 1: Standard Column
        Variable = NULL, # Layer 2: Variable Manager
        Level = NULL # Layer 3: Categorical Reference
    )

    uploadDataset <- reactiveValues(
        FromImport = FALSE,
        UUID = NULL,
        RightColumnRaw = NULL,
        Raw = NULL,
        RightColumnClean = NULL,
        Clean = NULL
    )

    uploadAlert <- reactiveValues(
        Message = NULL
    )

    uploadInputAction <- reactiveValues(
        Import = 0,
        Demo = 0
    )


    ## Step 1: Data Import
    ### import dataset
    saveConfirmModal <- function() {
        modalDialog(
            tags$h4('Do you want a share link?'),
            tags$h5('(The SMART will save your data.)'),
            footer = tagList(
                modalButton("No"),
                actionButton("saveConfirm", "Yes")
            ),
            easyClose = TRUE
        )
    }

    uploadRawDataset <- eventReactive(list(input$uploadStartAnalysisAction, input$uploadDemoAnalysisAction), {
        withProgress(
            message = 'Upload your data.',
            detail = "Start read dataset.",
            value = 0,
            expr = {
                rawData <- NULL
                overall <- NULL
                variable <- NULL
                rawLevel <- NULL
                checkResult <- list(FALSE, NULL)

                if (input$uploadStartAnalysisAction > uploadInputAction$Import) {
                    uploadInputAction$Import <- input$uploadStartAnalysisAction
                    rawData <- readUploadFile(input$uploadRawDataset, "dataset")
                    incProgress(0.1, detail = paste("Read dataset down, start read layer 1 metadata."))
                    overall <- readUploadFile(input$uploadOverallMetadata, "layer1 metadata")
                    incProgress(0.05, detail = paste("Read layer 1 metadata down, start read layer 2 metadata."))
                    variable <- readUploadFile(input$uploadVariableMetadata, "layer2 metadata")
                    incProgress(0.05, detail = paste("Read layer 2 metadata down, start read layer 3 metadata."))
                    rawLevel <- readUploadFile(input$uploadLevelMetadata, "layer3 metadata")
                    incProgress(0.1, detail = paste("Read layer 3 metadata down, start check your data."))

                    checkResult <- checkUploadDataset(overall, variable, rawLevel, rawData)
                    incProgress(0.1, detail = paste("Check your data down, start check whether save your data."))

                    if (checkResult[[1]]) {
                        showModal(saveConfirmModal())
                        uploadDataset$FromImport <- TRUE
                    }
                } else if (input$uploadDemoAnalysisAction > uploadInputAction$Demo) {
                    uploadInputAction$Demo <- input$uploadDemoAnalysisAction
                    rawData <- survival::colon
                    overall <- survivalPackageOverall("colon")
                    variable <- survivalPackageVariable("colon")
                    rawLevel <- survivalPackageLevel("colon")

                    checkResult <- list(TRUE, NULL)
                    setProgress(0.4)
                }

                if (checkResult[[1]]) {
                    reactiveDataList <- list(uploadAlert, uploadMetadata, uploadDataset)
                    yourDataList <- list(rawData, overall, variable, rawLevel)
                    loadDataResult <- loadYourData(session, reactiveDataList, yourDataList)
                    uploadAlert <- loadDataResult[[1]]
                    uploadMetadata <- loadDataResult[[2]]
                    uploadDataset <- loadDataResult[[3]]
                } else {
                    # the upload file has some error message.
                    uploadAlert$Message <- checkResult[[2]]
                    setProgress(1)
                }
            }
        )
    })

    ### Alert message
    output$uploadDataImportAlert <- renderUI({
        uploadRawDataset()
        if (!is.null(uploadAlert$Message)) {
            message <- tagList()
            for (msg in uploadAlert$Message) {
                message <- tagList(message, tags$h3(msg))
            }
            tags$div(
                message,
                class = "alert alert-dismissible alert-danger"
            )
        }
    })

    ### short access URL
    uploadDataURL <- reactive({
        saveConfirm <- input$saveConfirm
        if (is.null(saveConfirm)) {
            return(NULL)
        }
        if (!uploadDataset$FromImport) {
            return(NULL)
        }
        if (0 == saveConfirm[1]) {
            return(NULL)
        }
        removeModal()

        uploadDataset$UUID <- saveUploadDataset(uploadMetadata$Overall, uploadMetadata$Variable, uploadMetadata$Level, uploadDataset$Raw)
        paste0(
            session$clientData$url_protocol, "//",
            session$clientData$url_hostname, ":",
            session$clientData$url_port,
            "?id=", uploadDataset$UUID
        )
    })

    ### After click Import or Demo, the SMART will show Dataset and Metadata.
    output$uploadDataImportUI <- renderUI({
        if (!is.null(uploadDataset$Raw)) {
            uploadDataImportUI(uploadDataURL())
        }
    })

    ### show dataset
    output$uploadDatasetBrowser <- renderDataTable({
        uploadDataset$Raw
    }, options = function() {
        dataTableOption(
            pageLength = 10,
            scrollY = NULL,
            rightColumn = uploadDataset$RightColumnRaw
        )
    })

    ### show metadata
    output$uploadOverallMetadata <- renderDataTable({
        overall <- uploadMetadata$Overall
        overall$Description <- c("Overall Survival Time", "Flag for Death or alive (censor) (Death = 1)")
        overall
    }, options = dataTableOption(paging = FALSE, searching = FALSE))

    output$uploadVariableMetadata <- renderDataTable({
        uploadMetadata$Variable
    }, options = dataTableOption(paging = FALSE, searching = FALSE))

    output$uploadLevelMetadata <- renderDataTable({
        uploadMetadata$Level
    }, options = dataTableOption(paging = FALSE, searching = FALSE))

    ### download example data and metadata
    output$downloadExampleDataset <- downloadHandler(
        filename = function() {
            "Dataset.csv"
        },
        content = function(file) {
            write.table(survival::colon, file, sep = ",", row.names = FALSE)
        }
    )

    output$downloadExampleOverallMetadata <- downloadHandler(
        filename = function() {
            "Layer1.csv"
        },
        content = function(file) {
            example <- survivalPackageOverall("colon")
            write.table(example, file, sep = ",", row.names = FALSE)
        }
    )

    output$downloadExampleVariableMetadata <- downloadHandler(
        filename = function() {
            "Layer2.csv"
        },
        content = function(file) {
            example <- survivalPackageVariable("colon")
            write.table(example, file, sep = ",", row.names = FALSE)
        }
    )

    output$downloadExampleLevelMetadata <- downloadHandler(
        filename = function() {
            "Layer3.csv"
        },
        content = function(file) {
            example <- survivalPackageLevel("colon")
            write.table(example, file, sep = ",", row.names = FALSE)
        }
    )



    ## Step 2: Data Filter
    ### Step I: Filter option
    output$uploadFilterOptions <- renderUI({
        uploadFilterOption(uploadMetadata$Variable, uploadMetadata$Level)
    })

    ### Step II: Click filter button to generate clean data
    uploadFilterActionResult <- eventReactive(input$uploadFilterAction, {
        result <- uploadFilterAction(
            input = input,
            dataset = uploadDataset$Raw,
            variable = uploadMetadata$Variable,
            level = uploadMetadata$Level
        )
        raw <- result[[1]]
        clean <- extractTransformLoad(raw, uploadMetadata)
        uploadDataset$RightColumnClean <- numericTypeList(clean)
        uploadDataset$Clean <- clean
        result
    })

    ### Step III: Filter History
    output$uploadDatasetFilterCountTable <- renderTable({
        uploadFilterActionResult()[[2]][[1]]
    })

    ### Step IV: Ckeck clean dataset
    output$uploadCleanDataset <- renderDataTable({
        uploadDataset$Clean
    }, options = function() {
        dataTableOption(
            pageLength = 10,
            scrollY = NULL,
            rightColumn = uploadDataset$RightColumnClean
        )
    })



    ## Step 3: Descriptive Statistics
    ### demographic
    output$uploadDemographicOptionList <- renderUI({
        dynamicDemographicOption(uploadMetadata$Variable, uploadMetadata$Level, "upload")
    })

    output$uploadContinuousDemographic <- renderDataTable({
        continuousDemographic(
            input = input,
            dataset = uploadDataset$Clean,
            variable = uploadMetadata$Variable,
            level = uploadMetadata$Level,
            type = "upload"
        )
    }, options = dataTableOption())

    output$uploadCategoricalDemographic <- renderDataTable({
        categoricalDemographic(
            input = input,
            dataset = uploadDataset$Clean,
            variable = uploadMetadata$Variable,
            level = uploadMetadata$Level,
            type = "upload"
        )
    }, options = dataTableOption())


    ### pivot table (contingency table or cross tabulation)
    output$uploadContingencyTableOption <- renderUI({
        contingencyTableOption("upload")
    })

    output$uploadContingencyTableXaxis <- renderUI({
        contingencyTableXaxis(categoricalAxis(uploadMetadata$Variable, TRUE), "upload")
    })

    output$uploadContingencyTableYaxis <- renderUI({
        contingencyTableYaxis(categoricalAxis(uploadMetadata$Variable), "upload")
    })

    output$uploadContingencyTable <- renderTable({
        updateContingencyTableContent(uploadDataset$Clean, input, "upload")
    }, rownames = TRUE, digits = 0)


    ### Epidemiology Statistic Calculator
    uploadTotalDeath <- reactive({
        count(dplyr::filter(uploadDataset$Clean, EventDeath == "Death"))[[1]]
    })

    uploadTotalNewCases <- reactive({
        count(uploadDataset$Clean)[[1]]
    })

    uploadDurationUnitInRate <- reactive({
        unit <- input$uploadRateTimeUnit
        if (is.null(unit)) {
            unit <- "day"
        }
        unit
    })

    uploadTotalTime <- reactive({
        totalTimeOrigin <- sum(uploadDataset$Clean$TimeOS)
        totalTime <- totalTimeOrigin
        if (!is.null(input$uploadRateIntervalType)) {
            if ("1" != input$uploadRateIntervalType) {
                timeTransformer <- paste0(
                    "totalTime <- totalTimeOrigin ",
                    input$uploadRateIntervalType
                )
                eval(parse(text = timeTransformer))
            }
        }
        totalTime
    })

    output$uploadTimeUnitOptionListInRate <- renderUI({
        timeUI <- dynamicTimeUnitOption(
            timeUnit = uploadDurationUnitInRate(),
            type = "uploadRate",
            option = "year"
        )
        timeUnitUI <- timeUI[[1]]
        timeOptionUI <- timeUI[[2]]
        tagList(
            timeUnitUI,
            timeOptionUI,
            selectizeInput(
                inputId   = paste0("uploadMultiplePersonTime"),
                label     = "Multiple Person-Time",
                selected  = 1000,
                choices   = c(
                    "        1" = 1,
                    "       10" = 10,
                    "      100" = 100,
                    "    1,000" = 1000,
                    "   10,000" = 10000,
                    "  100,000" = 100000,
                    "1,000,000" = 1000000
                )
            )
        )
    })

    output$uploadEpidemiologyAxis <- renderUI({
        epidemiologyAxis(categoricalAxis(uploadMetadata$Variable, showOverall = TRUE), "upload")
    })

    output$uploadEpidemiologyTable <- renderTable({
        epidemiologyTable(
            dataset = uploadDataset$Clean,
            level = uploadMetadata$Level,
            input = input,
            type = "upload"
        )
    }, rownames = TRUE)



    ## Step 4: Survival Analysis
    ### Survival Formula Options
    uploadDatasetName <- reactive({
        input$uploadShowDatasetName
    })

    uploadDurationUnit <- reactive({
        unit <- input$uploadTimeUnit
        if (is.null(unit)) {
            unit <- "day"
        }
        unit
    })

    output$uploadFactorOptionList <- renderUI({
        dynamicFactorOption(
            categoricalAxis = categoricalAxis(uploadMetadata$Variable),
            continuousAxis = continuousAxis(uploadMetadata$Variable),
            dataset = uploadDataset$Clean,
            type = "upload"
        )
    })

    output$uploadTimeUnitOptionList <- renderUI({
        timeUI <- dynamicTimeUnitOption(uploadDurationUnit(), "upload")
        timeUnitUI <- timeUI[[1]]
        timeOptionUI <- timeUI[[2]]
        tagList(
            tags$div(class = "col-lg-4",
                timeUnitUI
            ),
            tags$div(class = "col-lg-8",
                timeOptionUI
            )
        )
    })

    ### Survival Formula
    uploadSurvivalFormulaText <- reactive({
        dynamicSurvivalFormula(input, "upload")
    })

    uploadSurvivalFormula <- reactive({
        eval(parse(text = uploadSurvivalFormulaText()))
    })

    output$uploadSurvivalFormula <- renderText({
        paste0("Survival Formula: ", uploadSurvivalFormulaText())
    })

    ### Survival Curve
    uploadShowCurveHeight <- reactive({
        curveHeight <- input$uploadShowCurveHeight
        ifelse((is.null(curveHeight)), 500, curveHeight)
    })

    output$uploadSurvivalCurveUI <- renderUI({
        show <- survivalAnalysisShowSidebarPanel()
        if (!updateIsShowSurvivalCurve()) {
            return(NULL)
        }

        tags$fieldset(id = "uploadSurvivalCurveFieldset", class = "singleLine",
            tags$legend("Survival Curve", class = "singleLine"),
            plotOutput(
                outputId = "uploadSurvivalCurve",
                width = "100%",
                height = paste0(uploadShowCurveHeight(), "px")
            )
        )
    })

    output$uploadSurvivalCurve <- renderPlot({
        show <- survivalAnalysisShowSidebarPanel()
        if (!updateIsShowSurvivalCurve()) {
            return(NULL)
        }

        renderSurvivalCurve(
            input = input,
            formula = uploadSurvivalFormula(),
            dataset = uploadDataset$Clean,
            type = "upload",
            durationUnit = uploadDurationUnit(),
            datasetName = uploadDatasetName()
        )
    })

    ### cox proportional hazards model
    output$uploadCoxTableOptionList <- renderUI({
        dynamicCoxTableOption(input, uploadMetadata, "upload")
    })

    uploadUniVariateCoxTable <- reactive({
        showUniVariateCoxTable(
            input = input,
            duration = "TimeOS",
            event = "EventDeath",
            dataset = uploadDataset$Clean,
            variable = uploadMetadata$Variable,
            type = "upload"
        )
    })

    output$uploadUniVariateCoxModelTitle <- renderText({
        if (!is.null(uploadUniVariateCoxTable())) {
            "Univariate analysis"
        }
    })

    output$uploadUniVariateCoxModelTable <- renderTable({
        uploadUniVariateCoxTable()
    }, rownames = TRUE, align = "lrlrr")

    uploadMultiVariateCoxTable <- reactive({
        showMultiVariateCoxTable(
            input = input,
            survivalFormula = uploadSurvivalFormula(),
            dataset = uploadDataset$Clean,
            variable = uploadMetadata$Variable,
            type = "upload"
        )
    })

    output$uploadMultiVariateCoxModelTitle <- renderText({
        if (!is.null(uploadMultiVariateCoxTable()[[2]])) {
            paste0(
                "Multivariate analysis (Log-rank test = ",
                uploadMultiVariateCoxTable()[[2]],
                ")"
            )
        }
    })

    output$uploadMultiVariateCoxModelTable <- renderTable({
        uploadMultiVariateCoxTable()[[1]]
    }, rownames = TRUE, align = "lrlr")

    survivalAnalysisShowSidebarPanel <- reactive({
        input$survivalAnalysisShowSidebarPanel
    })


    ### Survival Curve Options
    updateIsShowSurvivalCurve <- reactive({
        isShowSurvivalCurve(
            input = input,
            dataset = uploadDataset$Clean,
            variable = uploadMetadata$Variable,
            type = "upload"
        )
    })

    output$uploadSurvivalCurveOptionList <- renderUI({
        isShow <- ifelse(updateIsShowSurvivalCurve(), "show", "hide")
        dynamicCurveOption(isShow, "upload")
    })



    ## Step A: Variable Appender
    preInputAction <- reactiveValues(
        Import = 0,
        Demo = 0
    )

    preDataset <- reactiveValues(
        RightColumnRaw = NULL,
        Raw = NULL,
        Processed = NULL,
        RightColumnFinal = NULL,
        Final = NULL
    )

    ### Step I: Upload raw dataset
    preRawDataset <- eventReactive(list(input$preImportAction, input$preDemoAction), {
        rawData <- NULL
        withProgress(
            message = 'Step A.2: Check raw dataset.',
            detail = "Read upload dataset start.",
            value = 0,
            expr = {
                if (input$preImportAction > preInputAction$Import) {
                    preInputAction$Import <- input$preImportAction
                    rawData <- readUploadFile(input$preUploadRawDataset, "dataset")
                } else if (input$preDemoAction > preInputAction$Demo) {
                    preInputAction$Demo <- input$preDemoAction
                    rawData <- survival::colon
                    # rawData <- survival::jasa
                }
                incProgress(0.5, detail = paste("Read upload dataset down."))
                preDataset$Raw <- rawData
                preDataset$Processed <- rawData
                incProgress(0.25, detail = paste("Transfer dataset down, start transfer column."))
                preDataset$RightColumnRaw <- numericTypeList(rawData)
                incProgress(0.25, detail = paste("Transfer column down."))
                preRawChoicesUI(input, session, colnames(rawData))
            }
        )
        rawData
    })

    ### Step II: Check raw dataset
    output$preRawDatasetBrowser <- renderDataTable({
        preRawDataset()
    }, options = function() {
        dataTableOption(
            pageLength = 10,
            scrollY = NULL,
            rightColumn = preDataset$RightColumnRaw
        )
    })

    ### Step III: Time appender
    observeEvent(input$preGenerateDuration, {
        preDataset$Processed <- preDurationGenerator(input, preDataset$Processed)
        preProcessedChoicesUI(
            input = input,
            session = session,
            choices = colnames(preDataset$Processed),
            newColumn = input$preDurationColumnName
        )
        preDataset$Final <- preDataset$Processed[,input$preColumnSelector]
    })

    ### Step IV: Event appender
    observeEvent(input$preGenerateEvent, {
        preDataset$Processed <- preEventGenerator(input, preDataset$Processed)
        preProcessedChoicesUI(
            input = input,
            session = session,
            choices = colnames(preDataset$Processed),
            newColumn = input$preEventColumnName
        )
        preDataset$Final <- preDataset$Processed[,input$preColumnSelector]
    })

    ### Step V: Column selector
    observeEvent(input$preColumnSelector, {
        finalData <- preDataset$Processed[,input$preColumnSelector]
        preDataset$RightColumnFinal <- numericTypeList(finalData)
        preDataset$Final <- finalData
    })

    ### Step VI: Processed dataset
    output$preProcessedDatasetBrowser <- renderDataTable({
        preDataset$Final
    }, options = function() {
        dataTableOption(
            pageLength = 10,
            scrollY = NULL,
            rightColumn = preDataset$RightColumnFinal
        )
    })

    ### Step VII: Download Processed Dataset
    output$downloadProcessedDataset <- downloadHandler(
        filename = function() {
            nowTime <- format(Sys.time(), "_%Y%m%d_%H%M%S")
            paste0("yourProcessedDataset", nowTime, ".csv")
        },
        content = function(file) {
            table <- preDataset$Final
            write.table(table, file, sep = ",", row.names = FALSE)
        }
    )




    ## Step B: Metadata Generator
    metadataDataset <- reactiveValues(
        RightColumn = NULL,
        Clean = NULL
    )

    metadataInputAction <- reactiveValues(
        Import = 0,
        Demo = 0
    )

    ### Step I: Upload dataset
    metadataCleanDatasetName <- reactive({
        name <- input$metadataCleanDatasetName
        ifelse("" == name, "your", name)
    })

    metadataCleanDataset <- eventReactive(list(input$metadataImportAction, input$metadataDemoAction), {
        cleanData <- NULL
        withProgress(
            message = 'Step B.2: Check dataset.',
            detail = "Read upload dataset start.",
            value = 0,
            expr = {
                if (input$metadataImportAction > metadataInputAction$Import) {
                    metadataInputAction$Import <- input$metadataImportAction
                    cleanData <- readUploadFile(input$metadataCleanDataset, "dataset")
                } else if (input$metadataDemoAction > metadataInputAction$Demo) {
                    metadataInputAction$Demo <- input$metadataDemoAction
                    cleanData <- survival::colon
                    # cleanData <- survival::veteran
                }
                incProgress(0.5, detail = paste("Read upload dataset down."))
                metadataDataset$Clean <- cleanData
                incProgress(0.25, detail = paste("Transfer dataset down, start transfer column."))
                metadataDataset$RightColumn <- numericTypeList(cleanData)
                incProgress(0.25, detail = paste("Transfer column down."))
            }
        )
        cleanData
    })

    ### Step II: Check dataset
    output$metadataDatasetBrowser <- renderDataTable({
        metadataCleanDataset()
    }, options = function() {
        dataTableOption(
            pageLength = 10,
            scrollY = NULL,
            rightColumn = metadataDataset$RightColumn
        )
    })

    metadataColnames <- reactive({
        metadataColnames <- colnames(metadataDataset$Clean)
        if (isTRUE(all.equal(metadataColnames, c("Error Message")))) {
            metadataColnames <- NULL
        }
        metadataColnames
    })

    ### Step III: Layer 1
    output$fillInOverallMetadataUI <- renderUI({
        fillInOverallMetadataUI(metadataColnames())
    })

    output$fillInOverallMetadataDownloadUI <- renderUI({
        fillInDownloadUI(metadataCleanDatasetName(), "Layer1")
    })

    output$downloadYourLayer1Metadata <- downloadHandler(
        filename = function() {
            paste0(metadataCleanDatasetName(), "Layer1.csv")
        },
        content = function(file) {
            table <- fillInOverallMetadataContent(input)
            write.table(table, file, sep = ",", row.names = FALSE)
        }
    )

    ### Step IV: Layer 2
    output$fillInVariableMetadataUI <- renderUI({
        fillInVariableMetadataUI(metadataColnames())
    })

    output$fillInVariableMetadataDownloadUI <- renderUI({
        fillInDownloadUI(metadataCleanDatasetName(), "Layer2")
    })

    fillInVariableMetadata <- reactive({
        fillInVariableMetadataContent(input, metadataColnames())
    })

    output$downloadYourLayer2Metadata <- downloadHandler(
        filename = function() {
            paste0(metadataCleanDatasetName(), "Layer2.csv")
        },
        content = function(file) {
            layer2 <- fillInVariableMetadata()
            write.table(layer2, file, sep = ",", row.names = FALSE)
        }
    )

    fillInCategoracalColname <- eventReactive(
        input$transferVariableTypeToLevelMetadata,
        {
            layer2 <- fillInVariableMetadata()
            layer2[layer2$Type %in% c("Categorical", "Nominal", "Ordinal"), "Colname"]
        }
    )

    ### Step V: Layer 3
    output$fillInLevelMetadataUI <- renderUI({
        fillInLevelMetadataUI(
            categoricalColnames = fillInCategoracalColname(),
            dataset = metadataDataset$Clean
        )
    })

    output$fillInLevelMetadataDownloadUI <- renderUI({
        fillInDownloadUI(metadataCleanDatasetName(), "Layer3")
    })

    output$downloadYourLayer3Metadata <- downloadHandler(
        filename = function() {
            paste0(metadataCleanDatasetName(), "Layer3.csv")
        },
        content = function(file) {
            table <- fillInLevelMetadataContent(
                input = input,
                categoricalColnames = fillInCategoracalColname(),
                dataset = metadataDataset$Clean
            )
            write.table(table, file, sep = ",", row.names = FALSE)
        }
    )






    # embedded panel
    ## embedded metadata
    embeddedMetadata <- reactiveValues(
        Overall = NULL,
        Variable = NULL,
        Level = NULL
    )

    ## embedded dataset
    embeddedDataset <- reactiveValues(
        Raw = NULL,
        Clean = NULL
    )

    ## parse metadata
    observeEvent(input$embeddedChoicedDataset, {
        choices <- input$embeddedChoicedDataset
        rawData <- survivalPackageRawData(choices)
        embeddedDataset$Raw <- rawData
        embeddedMetadata$Overall <- survivalPackageOverall(choices)
        embeddedMetadata$Variable <- survivalPackageVariable(choices)
        rawLevel <- survivalPackageLevel(choices)
        embeddedMetadata$Level <- appendOriginLevel(rawData, embeddedMetadata$Variable, rawLevel)

    })

    embeddedOverallSetting <- reactive({
        transferOverallMetadata(embeddedMetadata$Overall)
    })

    ## show metadata
    output$embeddedOverallMetadata <- renderDataTable({
        embeddedMetadata$Overall
    }, options = dataTableOption())

    output$embeddedVariableMetadata <- renderDataTable({
        embeddedMetadata$Variable
    }, options = dataTableOption())

    output$embeddedLevelMetadata <- renderDataTable({
        embeddedMetadata$Level
    }, options = dataTableOption())

    ## show filter options
    output$embeddedFilterOptions <- renderUI({
        survivalPackageFilterOption(
            filterOption = input$embeddedChoicedDataset,
            variable = embeddedMetadata$Variable,
            level = embeddedMetadata$Level
        )
    })

    ## use filter action to generate clean data
    embeddedFilterDataset <- eventReactive(input$embeddedFilterAction, {
        result <- survivalPackageFilterAction(
            input = input,
            variable = embeddedMetadata$Variable,
            level = embeddedMetadata$Level
        )
        filtered <- result[[1]]
        embeddedDataset$Clean <- extractTransformLoad(filtered, embeddedMetadata)
        result
    })

    ## show clean flow
    output$embeddedDatasetFilterCountTable <- renderTable({
        embeddedFilterDataset()[[2]][[1]]
    })

    ## show clean data
    output$embeddedCleanDataShowVariableCheckbox <- renderUI({
        showVariableNameUI(input, embeddedMetadata$Variable, "embedded")
    })

    embeddedSelectedCleanDataset <- reactive({
        showCleanDataset(input, embeddedDataset$Clean, embeddedMetadata$Variable, "embedded")
    })

    output$embeddedCleanData <- renderDataTable({
        embeddedSelectedCleanDataset()
    }, options = dataTableOption())

    output$embeddedCleanDataDownload <- downloadHandler(
        filename = function() {
            nowTime <- format(Sys.time(), "_%Y%m%d_%H%M%S")
            paste0(input$embeddedChoicedDataset, "CleanData", nowTime, ".csv")
        },
        content = function(file) {
            write.table(embeddedSelectedCleanDataset(), file, sep = ",", row.names = FALSE)
        }
    )

    ## contingency table
    embeddedAxisList <- reactive({
        categoricalAxis(embeddedMetadata$Variable)
    })

    output$embeddedContingencyTableOption <- renderUI({
        contingencyTableOption("embedded")
    })

    output$embeddedContingencyTableXaxis <- renderUI({
        contingencyTableXaxis(categoricalAxis(embeddedMetadata$Variable, TRUE), "embedded")
    })

    output$embeddedContingencyTableYaxis <- renderUI({
        contingencyTableYaxis(embeddedAxisList(), "embedded")
    })

    output$embeddedContingencyTable <- renderTable({
        updateContingencyTableContent(embeddedDataset$Clean, input, "embedded")
    }, rownames = TRUE, digits = 0)

    ## demographic
    output$embeddedDemographicOptionList <- renderUI({
        dynamicDemographicOption(embeddedMetadata$Variable, embeddedMetadata$Level, "embedded")
    })

    output$embeddedCategoricalDemographic <- renderDataTable({
        categoricalDemographic(
            input = input,
            dataset = embeddedDataset$Clean,
            variable = embeddedMetadata$Variable,
            level = embeddedMetadata$Level,
            type = "embedded"
        )
    }, options = dataTableOption())

    output$embeddedContinuousDemographic <- renderDataTable({
        continuousDemographic(
            input = input,
            dataset = embeddedDataset$Clean,
            variable = embeddedMetadata$Variable,
            level = embeddedMetadata$Level,
            type = "embedded"
        )
    }, options = dataTableOption())

    ## survival formula
    embeddedDatasetName <- reactive({
        input$embeddedShowDatasetName
    })

    embeddedDurationUnit <- reactive({
        unit <- input$embeddedTimeUnit
        if (is.null(unit)) {
            unit <- "day"
        }
        unit
    })

    output$embeddedFactorOptionList <- renderUI({
        dynamicFactorOption(
            categoricalAxis = categoricalAxis(embeddedMetadata$Variable),
            continuousAxis = continuousAxis(embeddedMetadata$Variable),
            dataset = embeddedDataset$Clean,
            type = "embedded"
        )
    })

    output$embeddedTimeUnitOptionList <- renderUI({
        timeUI <- dynamicTimeUnitOption(embeddedDurationUnit(), "embedded")
        timeUnitUI <- timeUI[[1]]
        timeOptionUI <- timeUI[[2]]
        tagList(
            tags$div(class = "col-lg-4",
                timeUnitUI
            ),
            tags$div(class = "col-lg-8",
                timeOptionUI
            )
        )
    })

    embeddedSurvivalFormulaText <- reactive({
        dynamicSurvivalFormula(input, "embedded")
    })

    embeddedSurvivalFormula <- reactive({
        eval(parse(text = embeddedSurvivalFormulaText()))
    })

    output$embeddedSurvivalFormula <- renderText({
        paste0("Survival Formula: ", embeddedSurvivalFormulaText())
    })

    ## survival curve
    output$embeddedSurvivalCurveOptionList <- renderUI({
        dynamicCurveOption(input$embeddedShowSurvivalCurve, "embedded")
    })

    embeddedShowCurveHeight <- reactive({
        curveHeight <- input$embeddedShowCurveHeight
        ifelse((is.null(curveHeight)), 500, curveHeight)
    })

    output$embeddedSurvivalCurveUI <- renderUI({
        plotOutput(
            outputId = "embeddedSurvivalCurve",
            height = paste0(embeddedShowCurveHeight(), "px")
        )
    })

    output$embeddedSurvivalCurve <- renderPlot({
        renderSurvivalCurve(
            input = input,
            formula = embeddedSurvivalFormula(),
            dataset = embeddedDataset$Clean,
            type = "embedded",
            durationUnit = embeddedDurationUnit(),
            datasetName = embeddedDatasetName()
        )
    })

    ## show or hide
    output$embeddedShowPartOptionList <- renderUI({
        dynamicShowPartOption("embedded")
    })

    observeEvent(input$embeddedShowPartCheckbox, {
        for (outputName in showPartChoices("embedded")) {
            if (outputName %in% input$embeddedShowPartCheckbox) {
                shinyjs::show(outputName)
                shinyjs::enable(outputName)
            } else {
                shinyjs::hide(outputName)
                shinyjs::disable(outputName)
            }
        }
    }, ignoreNULL = FALSE)









    # use html anchor to link specific tab panel
    # https://github.com/rstudio/shiny/issues/772#issuecomment-112919149
    observe({
        query <- parseQueryString(session$clientData$url_search)
        dataId <- query$id
        if (!is.null(dataId)) {
            path <- paste0(getwd(), "/data/", dataId)
            withProgress(
                message = 'Restore your data.',
                detail = "Start read dataset.",
                value = 0,
                expr = {
                    rawData <- readUploadFile(paste0(path, "Dataset.csv"), "dataset", TRUE)
                    incProgress(0.1, detail = paste("Read dataset down, start read layer 1 metadata."))
                    overall <- readUploadFile(paste0(path, "Layer1.csv"), "layer1 metadata", TRUE)
                    incProgress(0.05, detail = paste("Read layer 1 metadata down, start read layer 2 metadata."))
                    variable <- readUploadFile(paste0(path, "Layer2.csv"), "layer2 metadata", TRUE)
                    incProgress(0.05, detail = paste("Read layer 2 metadata down, start read layer 3 metadata."))
                    rawLevel <- readUploadFile(paste0(path, "Layer3.csv"), "layer3 metadata", TRUE)
                    incProgress(0.1, detail = paste("Read layer 3 metadata down, start check data."))

                    checkResult <- checkUploadDataset(overall, variable, rawLevel, rawData)
                    incProgress(0.1, detail = paste("Check data down!"))
                    if (checkResult[[1]]) {
                        uploadDataset$UUID <- dataId
                        reactiveDataList <- list(uploadAlert, uploadMetadata, uploadDataset)
                        yourDataList <- list(rawData, overall, variable, rawLevel)
                        loadDataResult <- loadYourData(session, reactiveDataList, yourDataList)
                        uploadAlert <- loadDataResult[[1]]
                        uploadMetadata <- loadDataResult[[2]]
                        uploadDataset <- loadDataResult[[3]]
                    } else {
                        uploadAlert$Message <- paste0("The SMART could not find your id. (", dataId, ")")
                        setProgress(1)
                    }
                }
            )
        }
        specificTab <- query$tab
        if (!is.null(specificTab)) {
            updateNavbarPage(session, "navigator", selected = specificTab)
        }

        html(
            id = "smartTitle",
            html = HTML(
                paste0(
                    "<a id='smartTitle' href='",
                    session$clientData$url_protocol, "//",
                    session$clientData$url_hostname, ":",
                    session$clientData$url_port,
                    "'>SMART</a>"
                )
            )
        )
    })
}
