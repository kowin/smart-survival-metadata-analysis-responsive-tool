library(shiny)
library(shinyjs)
library(shinythemes)

source("includeUI.r", encoding = "UTF-8")

ui <- bootstrapPage(
    theme = shinytheme("readable"),
    tags$head(tags$link(rel = "stylesheet", type = "text/css", href = "smart.css")),
    tags$head(tags$link(rel = "shortcut icon", href = "smart.ico")),
    tags$head(tags$script(src = "saveConfirm.js")),
    shinyjs::useShinyjs(),
    navbarPage(
        windowTitle = "SMART",
        title = HTML("<a id='smartTitle'>SMART</a>"),
        dataImportTabPanel,
        dataFilterTabPanel,
        descriptiveStatisticsTabPanel,
        survivalAnalysisTabPanel,
        navbarMenu(
            title = tagList(
                tags$span("Advance", class = "tabPanelSmall"),
                tags$br(),
                "More",
                tags$br(),
                "Features"
            ),
            variablesAppenderTabPanel,
            metadataTabPanel,
            embeddedTabPanel,
            tutorialTabPanel
        ),
        id = "navigator",
        position = "fixed-top",
        collapsible = TRUE
    )
)
