FROM quantumobject/docker-shiny

MAINTAINER kowin olderbrothers@gmail.com

RUN R -e "install.packages(c('shinyjs','shinythemes','survival','survminer','dplyr','ggplot2','ggfortify','nortest','abind','uuid','readxl'),repos='http://cran.rstudio.com/')"
