# SMART - Survival Metadata Analysis Responsive Tool

## About SMART [(Public Website)](http://140.112.30.202:3838/ "SMART Website")

 SMART is a free software and a highly-responsive survival analysis tool.
 It includes all the features designed for clinical time-to-event analysis.
 This tool uses a metadata architecture that free your typing burden.
 For researchers, the SMART is a good hypothesis iteration tool, and does not learn any commands.
 We believe it would be useful in accelerating the site specific factors discovery in cancer research.

## Run SMART on [R Studio](https://www.rstudio.com/ "R Studio")

### Environment

1. Download and Install [R](https://cran.rstudio.com/ "Download R").
2. Download and Install [RStudio Desktop](https://www.rstudio.com/products/rstudio/download/ "Download R Studio").
3. In R Studio, install following packages.

```R
install.packages('shiny',repos='http://cran.rstudio.com/')
install.packages('shinyjs',repos='http://cran.rstudio.com/')
install.packages('shinythemes',repos='http://cran.rstudio.com/')
install.packages('survival',repos='http://cran.rstudio.com/')
install.packages('survminer',repos='http://cran.rstudio.com/')
install.packages('dplyr',repos='http://cran.rstudio.com/')
install.packages('ggplot2',repos='http://cran.rstudio.com/')
install.packages('ggfortify',repos='http://cran.rstudio.com/')
install.packages('nortest',repos='http://cran.rstudio.com/')
install.packages('abind',repos='http://cran.rstudio.com/')
install.packages('uuid',repos='http://cran.rstudio.com/')
install.packages('readxl',repos='http://cran.rstudio.com/')
```

You can also install packages by only one line command which on the below.

```R
install.packages(c('shiny','shinyjs','shinythemes','survival','survminer','dplyr','ggplot2','ggfortify','nortest','abind','uuid','readxl'),repos='http://cran.rstudio.com/')
```

### Use SMART

In R Studio, open `ui.r`, and click *Run App*. R Studio will open your browser and you can start your research.

### Clone from git

```bash
    sudo git clone https://bitbucket.org/kowin/smart-survival-metadata-analysis-responsive-tool.git LOCAL_DIRECTORY_PATH
```

### Update to newest version

```bash
    sudo git pull
```

## Run SMART on [Docker](https://www.docker.com/ "Docker")

* Build SMART

```bash
docker build -t smart:ICAT17 .
```

* Run SMART (replace *LOCAL_DIRECTORY_PATH* for your own path)

```bash
docker run -d \
    -p 3838:3838 \
    -v LOCAL_DIRECTORY_PATH:/srv/shiny-server/ \
    --name smart \
    -h smart \
    smart:ICAT17
```

* (option) If you want to keep share data.

```bash
docker run -d \
    -p 3838:3838 \
    -v LOCAL_DIRECTORY_PATH:/srv/shiny-server/ \
    -v LOCAL_DIRECTORY_PATH/data/:/srv/shiny-server/data/:Z \
    --name smart \
    -h smart \
    smart:ICAT17
```

* Use SMART by browser (replace *localhost* by your ip address)

```url
http:\\localhost:3838
```

* Stop SMART

```bash
docker stop  smart
```

* Remove SMART

```bash
docker rm -f smart
```
