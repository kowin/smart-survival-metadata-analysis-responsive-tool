
tutorialSidebarPanel <- sidebarPanel(
    tags$h3("Tutorial Outline"),
    tags$a(href = "#tutorialIntroduction", tags$h4("Introduction")),
    tags$a(href = "#tutorialStep1", tags$h4("Step 1: Data Import")),
    tags$a(href = "#tutorialStep2", tags$h4("Step 2: Data Filter")),
    tags$a(href = "#tutorialStep3", tags$h4("Step 3: Descriptive Statistics")),
    tags$a(href = "#tutorialStep4", tags$h4("Step 4: Survival Analysis")),
    tags$a(href = "#tutorialStepA", tags$h4("(Step A): Variable Appender")),
    tags$a(href = "#tutorialStepB", tags$h4("(Step B): Metadata Ganerator")),
    tags$a(href = "#tutorialEmbedded", tags$h4("(Advance): Embedded Dataset")),
    # tags$a(href = "#tutorialGlossary", tags$h4("Glossary")),
    tags$br(),
    logoList,
    tags$br(),
    class = "fixPanel"
)

tutorialMainPanel <- mainPanel(
    tags$h2("Survival Metadata Analysis Responsive Tool - Tutorial"),


    tags$a(id = "tutorialIntroduction", class = "anchor"),
    tags$h3("Introduction"),
    tags$ul(
        tags$li(
            tags$span(class = "h4", "About SMART"),
            tags$p("
 SMART (Survival Meta-Analysis Responsive Tool)
 is a free software and highly-responsive survival analysis tool.
 It includes all the features designed for survival analysis methods,
 and did more efficient than traditional.
 You can either greatly decrease your time spent studying,
 or greatly increase the productivity for biomedical researchers.
            "),
            tags$br()
        ),
        tags$li(
            tags$span(class = "h4", "SMART workflow"),
            tags$br(),
            tags$img(src = "workflow.png", id = "workflowImg"),
            tags$br(),
            tags$p("
 The SMART is designed for clinical researchers to perform the survival analysis and test their thesis.
 It consists of a standard survival analysis process flow and come along with two Metadata Utilities.
 The users can start a study from uploading the dataset and its metadata by Data Import (Step 1) for following analysis.
 After Extract-Transform-Load (ETL), researchers can choose their data by Data Filter (Step 2) according to their study hypothesis.
 Next, they can double-check their thesis by Descriptive Statistics (Step 3) and Survival Analysis (Step 4).
 The SMART uses three-layer metadata as input, and we develop Metadata Utilities for users.
 Users can append a column by Variable Appender (Step A), and make specific metadata by Metadata Generator (Step B).
 The SMART uses the R environment in the back end and the results are collected and demonstrated by the R Shiny Web.
 Based on the SMART architecture, analysts can make the analysis and verify their study hypothesis more easily.
            "),
            tags$br()
        ),
        tags$li(
            tags$span(class = "h4", "How to use the SMART?"),
            tags$br(),
            tags$p(
                tags$iframe(
                    "Loading…",
                    width = "100%",
                    height = "380",
                    src = "https://www.youtube.com/embed/JHpP04IWf6Q?rel=0",
                    frameborder = "0",
                    allowfullscreen = "true"
                )
            ),
            tags$br()
        ),
        tags$li(
            tags$span(class = "h4", "How to make your own metadata in the SMART?"),
            tags$br(),
            tags$p(
                tags$iframe(
                    "Loading…",
                    width = "100%",
                    height = "380",
                    src = "https://www.youtube.com/embed/7LEhIKqH4HI?rel=0",
                    frameborder = "0",
                    allowfullscreen = "true"
                )
            ),
            tags$br()
        )
    ),
    tags$a(id = "tutorialStep1", class = "anchor"),
    tags$h3("Step 1: Data Import"),
    tags$ul(
        tags$li(
            tags$span(class = "h4", "Demo"),
            tags$span(class = "h5", ": Import example data.")
        ),
        tags$li(
            tags$span(class = "h4", "Import"),
            tags$span(class = "h5", ": Browse your data, and click it.")
        ),
        tags$ul(
            tags$li(
                tags$span(class = "h5", "Dataset"),
                ": Survival analysis dataset."
            ),
            tags$li(
                tags$span(class = "h5", "Layer 1: Standard Column"),
                ": Describe your overall setting."
            ),
            tags$li(
                tags$span(class = "h5", "Layer 2: Variable Manager"),
                ": Describe every column information."
            ),
            tags$li(
                tags$span(class = "h5", "Layer 3: Categorical Reference"),
                ": Describe every categorical information for each column."
            )
        ),
        tags$li(
            tags$h4("Example data: "),
            "
The source file of metadata and dataset.
 We use colon cancer dataset in the survival package.
 The metadata of colon dataset is made by the ",
 tags$a("document", target = "_blank", href = "http://vincentarelbundock.github.io/Rdatasets/doc/survival/colon.html"),
 " of ",
 tags$a("RDataset.", target = "_blank", href = "https://vincentarelbundock.github.io/Rdatasets/")
        ),
        tags$li(
            tags$h4("Import check: "),
            "After you import your data, SMART will check whether your data fit the correct format."
        ),
        tags$li(
            tags$h4("Data browser: "),
            "Provider a simple data browser for check."
        )
    ),


    tags$a(id = "tutorialStep2", class = "anchor"),
    tags$h3("Step 2: Data Filter"),
    tags$ul(
        tags$li(
            tags$h4("Step I: Filter Option: "),
            "Filter choice control panel lines up all the categorical variable for researchers to define their unproved criteria."
        ),
        tags$li(
            tags$h4("Step II: Filter: "),
            "A filter button to kickoff filtering progress."
        ),
        tags$li(
            tags$h4("Step III: Filter History: "),
            "Filter history will announce the variation of case number by those filter definitions."
        ),
        tags$li(
            tags$h4("Step IV: Standardized Dataset: "),
            "Preview of uniform dataset for following survival analysis."
        )
    ),


    tags$a(id = "tutorialStep3", class = "anchor"),
    tags$h3("Step 3: Descriptive Statistics"),
    tags$ul(
        tags$li(
            tags$span("Demography", class = "h4"),
            tags$a("wiki",
                href = "https://en.wikipedia.org/wiki/Demography",
                target = "_blank"
            )
        ),
        tags$ul(
            tags$li(
                tags$h4("Stratification: "),
                "Stratification factor selection block for demographic."
            ),
            tags$li(
                tags$h4("Continuous: "),
                "Result of descriptive statistics for each explanatory continuous variable."
            ),
            tags$li(
                tags$h4("Categorical: "),
                "Result of descriptive statistics for each explanatory categorical variable."
            )
        ),
        tags$li(
            tags$span("Pivot Table", class = "h4"),
            tags$a("wiki",
                href = "https://en.wikipedia.org/wiki/Pivot_table",
                target = "_blank"
            ),
            tags$ul(
                tags$li(
                    tags$h5("Show Type: "),
                    "Choose number or percent of patient shows in pivot table."
                ),
                tags$li(
                    tags$h5("Row label: "),
                    "Choose the row variable in pivot table."
                ),
                tags$li(
                    tags$h5("Sum percent by: "),
                    "Shows the direction that sum percent."
                ),
                tags$li(
                    tags$h5("Column label: "),
                    "Choose the column variable in pivot table."
                ),
                tags$li(
                    tags$h5("Pivot table area: "),
                    "Show the pivot table."
                )
            )
        )
    ),


    tags$a(id = "tutorialStep4", class = "anchor"),
    tags$h3("Step 4: Survival Analysis"),
    tags$ul(
        tags$li(
            tags$h4("Survival Formula Options: "),
            tags$ul(
                tags$li(
                    tags$h5("Right hand side of formula: "),
                    "Selection box of survival formula."
                ),
                tags$li(
                    tags$h5("Time unit: "),
                    "You must choose the time unit of this dataset."
                ),
                tags$li(
                    tags$h5("Time unit transformer: "),
                    "The reveal time unit of Survival Curve."
                )
            )
        ),
        tags$li(
            tags$h4("Survival Formula: "),
            "Show the formula used in survival analysis."
        ),
        tags$li(
            tags$h4("Survival Curve Options: "),
            tags$ul(
                tags$li(
                    tags$h5("Curve Type: ")
                ),
                tags$ul(
                    tags$li("Kaplan-Meier",
                        tags$a("wiki",
                            href = "https://en.wikipedia.org/wiki/Kaplan%E2%80%93Meier_estimator",
                            target = "_blank"
                        )
                    ),
                    tags$li("Nelson-Aalen",
                        tags$a("wiki",
                            href = "https://en.wikipedia.org/wiki/Nelson%E2%80%93Aalen_estimator",
                            target = "_blank"
                        )
                    )
                ),
                tags$li(
                    tags$h5("Censor Data: "),
                    "Show or hide the censor data in the survival curve."
                ),
                tags$li(
                    tags$h5("P value: "),
                    "Show or hide the P value in the right hand side of the survival curve."
                ),
                tags$li(
                    tags$h5("Risk Table: "),
                    "Show or hide the risk table in the button of the survival curve."
                ),
                tags$li(
                    tags$h5("CI 95%: "),
                    "Show or hide the 95% confidence interval of the survival curve."
                ),
                tags$li(
                    tags$h5("Line Type: "),
                    "choose the solid or strata line shows in the survival curve."
                ),
                tags$li(
                    tags$h5("Survival Curve: "),
                    "Show or hide the whole survival curve."
                ),
                tags$li(
                    tags$h5("Show Dataset Name: "),
                    "Show the dataset name in the x axis."
                ),
                tags$li(
                    tags$h5("Show Curve Height: "),
                    "An Adjust bar to change the height of survival curve."
                )
            )
        ),
        tags$li(
            tags$h4("Survival Curve: "),
            "The area of survival curve."
        ),
        tags$li(
            tags$h4("Cox Table Reference Category: "),
            "Choose the base level, and change in the Hazard Ratio (HR) in the cox table."
        ),
        tags$li(
            tags$h4("Cox Table: "),
            tags$ul(
                tags$li(
                    tags$h5("Univariate analysis: "),
                    "Result of uni-variate Cox proportional hazard ratio analysis."
                ),
                tags$li(
                    tags$h5("Multivariate analysis: "),
                    "Result of multi-variate Cox proportional hazard ratio analysis."
                )
            )
        )
    ),


    tags$a(id = "tutorialStepA", class = "anchor"),
    tags$h3("(Step A): Variable Appender"),
    tags$ul(
        tags$li(
            tags$h4("Step A.1: Upload raw dataset"),
            "Import your dataset."
        ),
        tags$li(
            tags$h4("Step A.2: Check raw dataset"),
            "A preview browser for you to check your dataset."
        ),
        tags$li(
            tags$h4("Step A.3: Time appender"),
            "
Specify start date column and end date column.
 The SMART computes end date minus start date, and append this value on the dataset.
            "
        ),
        tags$li(
            tags$h4("Step A.4: Event appender"),
            "
{Reference, operator, Value} is a boolean formula.
 When this formula is TURE, the SMART will give '1', others will be '0'.
 It is useful when you need to transform ALIVE to EventDeath.
 This funtion also useful when you need to split a continuous column.
 For example, Age > 61.
            "
        ),
        tags$li(
            tags$h4("Step A.5: Column selector"),
            "Choose the column what you want or you don't want."
        ),
        tags$li(
            tags$h4("Step A.6: Processed dataset"),
            "A previre browser for you to check processed dataset."
        ),
        tags$li(
            tags$h4("Step A.7: Download Processed dataset"),
            "A download button for you to download processed dataset."
        )
    ),


    tags$a(id = "tutorialStepB", class = "anchor"),
    tags$h3("(Step B): Metadata Ganerator"),
    tags$ul(
        tags$li(
            tags$h4("Step B.1: Upload dataset"),
            "Import your dataset. You can also fill dataset name for download name."
        ),
        tags$li(
            tags$h4("Step b.2: Check dataset"),
            "A preview browser for you to check your dataset."
        ),
        tags$li(
            tags$h4("Step B.3: Layer 1 (Standard Column)"),
            "
A layer 1 metadata template for you to fill in.
 TimeOS and EventDeath are both required column.
 You need to choose the corresponding column name.
 After you fill in metadata template, you need to click download button for download layer 1 metadata.
            "
        ),
        tags$li(
            tags$h4("Step B.4: Layer 2 (Variable Manager)"),
            "
A layer 2 metadata template for you to fill in.
 The type includes Nominal, Ordinal, Interval, and Ratio.
 Add new type: Categorical and Continuous.
 Categorical is an alias of Nominal.
 Continuous is an alias of Interval.
 The label will display along the SMART, stand for this categorical value.
 After you fill in metadata template, you need to click download button for download layer 2 metadata.
            "
        ),
        tags$li(
            tags$h4("Step B.5: Layer 3 (Categorical Reference)"),
            "
Please click 'Transfer layer 2 type' button, and then the SMART shows the template.
 A layer 3 metadata template for you to fill in.
 The value refers the categorical value of this column.
 The label will display along the SMART, stand for this categorical value.
 After you fill in metadata template, you need to click download button for download layer 3 metadata.
            "
        )
    ),


    tags$a(id = "tutorialEmbedded", class = "anchor"),
    tags$h3("(Advance): Embedded Dataset"),
    "Embedded Dataset includes open datasets in survival package, and collect all the function into one page. ",
    tags$br(),
    # tags$a(id = "tutorialGlossary", class = "anchor"),
    # tags$h3("Glossary"),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br(),
    tags$br()
)


tutorialTabPanel <- tabPanel(
    value = "Tutorial",
    title = tagList(
        tags$span("Tutorial", class = "h4"),
        tags$br(),
        "Teach",
        tags$br(),
        "Yourself"
    ),
    sidebarLayout(
        tutorialSidebarPanel,
        tutorialMainPanel
    )
)
