
variablesAppenderTabPanel <- tabPanel(
    value = "StepA",
    tagList(tags$span("(Step A)", class = "tabPanelSmall"), tags$br(), "Variable", tags$br(), "Appender"),
    tags$fieldset(id = "preUploadRawDatasetFieldset", class = "singleLine",
        tags$legend("Step A.1: Upload raw dataset", class = "singleLine"),
        tags$div(class = "col-sm-4",
            fileInput(
                inputId   = "preUploadRawDataset",
                label     = tags$span("Raw Dataset", class = "biggerFont"),
                accept    = acceptFileList()
            )
        ),
        tags$div(class = "col-sm-4",
            tags$span(class = "h5", "Import dataset"),
            actionButton(
                inputId   = "preImportAction",
                label     = tags$span("Import", class = "h4"),
                icon      = icon("open", lib = "glyphicon"),
                class     = "btn-primary btn-sm btn-block"
            )
        ),
        tags$div(class = "col-sm-4",
            tags$span(class = "h5", "Example dataset"),
            actionButton(
                inputId   = "preDemoAction",
                label     = tags$span("Demo", class = "h4"),
                class     = "btn-info btn-sm btn-block"
            )
        )
    ),
    tags$hr(),
    tags$fieldset(id = "preRawDatasetFieldset", class = "singleLine",
        tags$legend("Step A.2: Check raw dataset", class = "singleLine"),
        dataTableOutput(outputId = "preRawDatasetBrowser")
    ),
    tags$hr(),
    tags$fieldset(id = "preDurationGeneratorFieldset", class = "well",
        tags$legend("Step A.3: Time appender"),
        div(class = "col-sm-3",
            selectizeInput(
                inputId   = "preStartDateColumn",
                label     = "Start date column",
                choices   = c(" ")
            )
        ),
        div(class = "col-sm-3",
            selectizeInput(
                inputId   = "preEndDateColumn",
                label     = "End date column",
                choices   = c(" ")
            )
        ),
        div(class = "col-sm-3",
            textInput(
                inputId   = "preDurationColumnName",
                label     = "Time column name",
                value     = "TimeOS"
            )
        ),
        div(class = "col-sm-3",
            actionButton(
                inputId   = "preGenerateDuration",
                label     = "Append time",
                class     = "btn-lg btn-info"
            )
        )
    ),
    tags$hr(),
    tags$fieldset(id = "preEventGeneratorFieldset", class = "well",
        tags$legend("Step A.4: Event appender"),
        div(class = "col-sm-2",
            selectizeInput(
                inputId   = "preEventReferenceColumn",
                label     = "Reference",
                choices   = c(" ")
            )
        ),
        div(class = "col-sm-2",
            selectizeInput(
                inputId   = "preEventReferenceCompare",
                label     = "operator",
                choices   = c(" == ", " >= ", " <= ", " > ", " < ", " != ")
            )
        ),
        div(class = "col-sm-2",
            textInput(
                inputId   = "preEventReferenceValue",
                label     = "Value",
                value     = "0"
            )
        ),
        div(class = "col-sm-3",
            textInput(
                inputId   = "preEventColumnName",
                label     = "Event column name",
                value     = "EventDeath"
            )
        ),
        div(class = "col-sm-3",
            actionButton(
                inputId   = "preGenerateEvent",
                label     = "Append event",
                class     = "btn-lg btn-info"
            )
        )
    ),
    tags$hr(),
    tags$fieldset(id = "preColumnSelectorFieldset", class = "well",
        tags$legend("Step A.5: Column selector"),
        selectizeInput(
            inputId   = "preColumnSelector",
            label     = paste0(
                "Select the column tag that you want to analysis. ",
                "(Use delete key or backspace key to remove tag.)"
            ),
            multiple  = TRUE,
            choices   = c(" ")
        )
    ),
    tags$fieldset(id = "preProcessedDatasetFieldset", class = "singleLine",
        tags$legend("Step A.6: Processed dataset", class = "singleLine"),
        dataTableOutput(outputId = "preProcessedDatasetBrowser")
    ),
    tags$div(class = "row",
            tags$div(class = "col-md-6",
            downloadButton(
                outputId  = "downloadProcessedDataset",
                label = "Step A.7: Download Processed Dataset",
                class = "btn-primary btn-lg btn-block"
            )
        )
    ),
    tags$br(),
    logoList,
    tags$br()
)
