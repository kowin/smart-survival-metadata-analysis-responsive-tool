
embeddedSidebarPanel <- sidebarPanel(
    selectInput(
        inputId   = "embeddedChoicedDataset",
        label     = "Embedded dataset in Survival package",
        selected  = "colon",
        choices   = c(
            "bladder - Bladder Cancer Recurrences" = "bladder",
            "cancer - NCCTG Lung Cancer Data" = "cancer",
            "cgd - Chronic Granulotomous Disease data" = "cgd",
            "colon - Chemotherapy for Stage B/C colon cancer" = "colon",
            "flchain - Assay of serum free light chain for 7874 subjects." = "flchain",
            "genfan - Generator fans" = "genfan",
            "heart - Stanford Heart Transplant data" = "heart",
            "kidney - Kidney catheter data" = "kidney",
            "leukemia - Acute Myelogenous Leukemia survival data" = "leukemia",
            "logan - Data from the 1972-78 GSS data used by Logan" = "logan",
            "lung - NCCTG Lung Cancer Data" = "lung",
            "mgus - Monoclonal gammapothy data" = "mgus",
            "mgus2 - Monoclonal gammapothy data" = "mgus2",
            "nwtco - Data from the National Wilm's Tumor Study" = "nwtco",
            "ovarian - Ovarian Cancer Survival Data" = "ovarian",
            "pbc - Mayo Clinic Primary Biliary Cirrhosis Data" = "pbc",
            "rats - Rat treatment data from Mantel et al" = "rats",
            "retinopathy - Diabetic Retinopathy" = "retinopathy",
            "stanford2 - More Stanford Heart Transplant data" = "stanford2",
            "tobin - Tobin's Tobit data" = "tobin",
            "transplant - Liver transplant waiting list" = "transplant",
            "veteran - Veterans' Administration Lung Cancer study" = "veteran")
    ),
    uiOutput(outputId = "embeddedFilterOptions"),
    actionButton(inputId = "embeddedFilterAction", label = "Filter Dataset"),
    hr(),
    tableOutput(outputId = "embeddedDatasetFilterCountTable"),
    fluid = FALSE
)
