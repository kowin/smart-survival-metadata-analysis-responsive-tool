
dataImportTagList <- tagList(
    tags$div(class = "col-xs-12",
        buttonTypeRadioButton(
            inputId   = "dataImportShowSidebarPanel",
            label     = tags$h6("Data import panel"),
            inline    = TRUE,
            selected  = "Show",
            choices   = c("Show", "Hide")
        )
    ),

    uiOutput(outputId = "uploadDataImportUI")
)

dataImportTabPanel <- tabPanel(
    value = "Step1",
    tagList(tags$span("Step 1", class = "h4"), tags$br(), "Data", tags$br(), "Import"),
    conditionalPanel("input.dataImportShowSidebarPanel == 'Show'",
        sidebarLayout(
            uploadSidebarPanel,
            simpleTutorialPanel
        )
    ),
    dataImportTagList
)
