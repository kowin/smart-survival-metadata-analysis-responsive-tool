
simpleTutorialPanel <- mainPanel(
    uiOutput(outputId = "uploadDataImportAlert"),
    tags$h2("SMART - Survival Metadata Analysis Responsive Tool"),
    tags$br(),
    tags$span(class = "h4", "Prepare your metadata by "),
    tags$a(href = "/?tab=StepB",
        tags$span(class = "h4 text-success", "Metadata Generator.")
    ),
    tags$br(),
    tags$br(),
    tags$ul(
        tags$li(
            tags$span(class = "h3", "Import"),
            tags$span(class = "h4", ": Browse your data, and click it.")
        ),
        tags$li(
            tags$span(class = "h3", "Demo"),
            tags$span(class = "h4", ": Import example data, and follow navigator.")
        )
    ),
    tags$br(),
    tags$span(class = "h4", "Please read "),
    tags$a(href = "/?tab=Tutorial",
        tags$span(class = "h4 text-success", "Advance > Tutorial")
    ),
    tags$span(class = "h4", "if you want to learn more."),
    tags$br(),
    tags$br(),
    tags$fieldset(
        class = "singleLine",
        tags$legend(tags$span(class = "h3", "Cite Info"), class = "singleLine"),
        tags$span(class = "h4", "Please use Yuan-Chia Chu, Wen-Tsung Kuo, Yuan-Ren Cheng, Chung-Yuan Lee, Cheng-Ying Shiau, Der-Cherng Tarng & Feipei Lai (2018) Scientific Reports to cite \"A Survival Metadata Analysis Responsive Tool (SMART) for web-based analysis of patient survival and risk\"."),
        tags$br(),
        tags$span(class = "h4", "URL: "),
        tags$a(
            href = "https://www.nature.com/articles/s41598-018-31290-z",
            target = "_blank",
            tags$span(class = "h4 text-success", "https://www.nature.com/articles/s41598-018-31290-z")
        )
    ),
    tags$br(),
    tags$iframe("Loading…", width = "100%", height = "350", src = "https://docs.google.com/forms/d/e/1FAIpQLSeqAty3essq7Fana6b9YPsSrzT0pQsTXxPRBbRHBeRmBukUaQ/viewform?embedded=true"),
    tags$br(),
    tags$fieldset(class = "singleLine",
        tags$legend(tags$span(class = "h3", "About SMART"), class = "singleLine"),
        tags$p(class = "biggerFont", "
 SMART is a free software and a highly-responsive survival analysis tool.
 It includes all the features designed for clinical time-to-event analysis.
 This tool uses a metadata architecture that free your typing burden.
 For researchers, the SMART is a good hypothesis iteration tool, and does not learn any commands.
 We believe it would be useful in accelerating the site specific factors discovery in cancer research.
        ")
    ),
    tags$br(),
    logoList,
    tags$br()
)
