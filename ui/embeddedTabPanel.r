
embeddedMainPanel <- mainPanel(
    div(class = "well",
        uiOutput(outputId = "embeddedShowPartOptionList")
    ),
    h4(textOutput(outputId = "embeddedSurvivalFormula", inline = TRUE)),
    tags$fieldset(id = "embeddedSurvivalFormulaOptionFieldset", class = "well",
        tags$legend("Survival Formula Options"),
        uiOutput(outputId = "embeddedFactorOptionList"),
        uiOutput(outputId = "embeddedTimeUnitOptionList")
    ),
    hr(),
    tags$fieldset(id = "embeddedSurvivalCurveOptionFieldset", class = "well",
        tags$legend("Survival Curve Options"),
        uiOutput(outputId = "embeddedSurvivalCurveOptionList")
    ),
    tags$fieldset(id = "embeddedSurvivalCurveFieldset", class = "singleLine",
        tags$legend("Survival Curve", class = "singleLine"),
        uiOutput(outputId = "embeddedSurvivalCurveUI")
    ),
    hr(),
    tags$fieldset(id = "embeddedDemographicOptionFieldset", class = "well",
        tags$legend("Demographic Options"),
        uiOutput(outputId = "embeddedDemographicOptionList")
    ),
    tags$fieldset(id = "embeddedCategoricalDemographicFieldset", class = "singleLine",
        tags$legend("Categorical Demographic Browser (Nominal or Ordinal)", class = "singleLine"),
        dataTableOutput(outputId = "embeddedCategoricalDemographic")
    ),
    tags$fieldset(id = "embeddedContinuousDemographicFieldset", class = "singleLine",
        tags$legend("Continuous Demographic Browser (Interval or Ratio)", class = "singleLine"),
        dataTableOutput(outputId = "embeddedContinuousDemographic")
    ),
    hr(),
    tags$fieldset(id = "embeddedContingencyTableFieldset", class = "well",
        tags$legend("Pivot Table (Contingency Table or Cross Tabulation)"),
        div(class = "col-xs-2",
            uiOutput(outputId = "embeddedContingencyTableOption")
        ),
        div(class = "col-xs-10",
            uiOutput(outputId = "embeddedContingencyTableXaxis")
        ),
        div(class = "col-xs-2",
            uiOutput(outputId = "embeddedContingencyTableYaxis")
        ),
        div(class = "col-xs-10",
            tableOutput(outputId = "embeddedContingencyTable")
        )
    ),
    hr(),
    tags$fieldset(id = "embeddedCleanDataFieldset", class = "singleLine",
        tags$legend("Clean Dataset Browser", class = "singleLine"),
        div(class = "col-xs-2 well",
            downloadButton(
                outputId  = "embeddedCleanDataDownload",
                label = "Download"
            ),
            hr(),
            radioButtons(
                inputId   = "embeddedCleanDataShowVariableType",
                label     = "variable show type",
                selected  = "Label",
                choices   = c("short description" = "Label",
                              "raw column name" = "Colname")
            ),
            uiOutput(outputId = "embeddedCleanDataShowVariableCheckbox")
        ),
        div(class = "col-xs-10",
            dataTableOutput(outputId = "embeddedCleanData")
        )
    ),
    hr(),
    tags$fieldset(id = "embeddedOverallMetadataFieldset", class = "singleLine",
        tags$legend("Standard Column Browser (Layer 1 Metadata)", class = "singleLine"),
        dataTableOutput(outputId = "embeddedOverallMetadata")
    ),
    hr(),
    tags$fieldset(id = "embeddedVariableMetadataFieldset", class = "singleLine",
        tags$legend("Variable Manager Browser (Layer 2 Metadata)", class = "singleLine"),
        dataTableOutput(outputId = "embeddedVariableMetadata")
    ),
    hr(),
    tags$fieldset(id = "embeddedLevelMetadataFieldset", class = "singleLine",
        tags$legend("Categorical Reference Browser (Layer 3 Metadata)", class = "singleLine"),
        dataTableOutput(outputId = "embeddedLevelMetadata")
    ),
    tags$br(),
    logoList,
    tags$br(),
    fluid = FALSE
)

embeddedTabPanel <- tabPanel(
    value = "Advance",
    tagList(tags$span("(Advance)", class = "tabPanelSmall"), tags$br(), "Embedded", tags$br(), "Dataset"),
    # icon("wrench"),
    sidebarLayout(
        embeddedSidebarPanel,
        embeddedMainPanel,
        fluid = FALSE
    )
)
