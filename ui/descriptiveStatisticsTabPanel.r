
descriptiveStatisticsTabPanel <- tabPanel(
    value = "Step3",
    tagList(tags$span("Step 3", class = "h4"), tags$br(), "Descriptive", tags$br(), "Statistics"),

    tags$h3("Demography"),

    tags$div(class = "row",
        tags$div(class = "col-xs-6 col-xs-offset-3 col-lg-2 col-lg-offset-2",
            uiOutput(outputId = "uploadDemographicOptionList")
        )
    ),

    tags$div(class = "col-lg-6",
        tags$fieldset(id = "uploadContinuousDemographicFieldset", class = "singleLine",
            tags$legend("Continuous", class = "singleLine"),
            dataTableOutput(outputId = "uploadContinuousDemographic")
        )
    ),
    tags$div(class = "col-lg-6",
        tags$fieldset(id = "uploadCategoricalDemographicFieldset", class = "singleLine",
            tags$legend("Categorical", class = "singleLine"),
            dataTableOutput(outputId = "uploadCategoricalDemographic")
        )
    ),

    tags$hr(),
    tags$h3("Pivot Table"),
    tags$fieldset(id = "uploadContingencyTableFieldset", class = "well",
        tags$div(class = "col-sm-4 col-md-4 col-lg-3",
            uiOutput(outputId = "uploadContingencyTableOption")
        ),
        tags$div(class = "col-sm-8 col-md-8 col-lg-9",
            uiOutput(outputId = "uploadContingencyTableXaxis")
        ),
        tags$div(class = "col-sm-4 col-md-4 col-lg-3",
            uiOutput(outputId = "uploadContingencyTableYaxis")
        ),
        tags$div(class = "col-sm-8 col-md-8 col-lg-9",
            tableOutput(outputId = "uploadContingencyTable")
        )
    ),
    tags$br(),

    tags$h3("Epidemiology"),
    tags$fieldset(id = "uploadStatisticCalculatorFieldset", class = "singleLine",
        tags$legend("Mortality", class = "singleLine"),
        tags$div(class = "col-sm-4 col-md-3 col-lg-2",
            tags$br()
        ),
        tags$div(class = "col-sm-8 col-md-9 col-lg-10",
            uiOutput(outputId = "uploadEpidemiologyAxis")
        ),
        tags$div(class = "clearDiv"),
        tags$div(class = "col-sm-4 col-md-3 col-lg-2",
            uiOutput(outputId = "uploadTimeUnitOptionListInRate")
        ),
        tags$div(class = "col-sm-8 col-md-9 col-lg-10",
            uiOutput(outputId = "uploadEpidemiologyTable")
        )
    ),
    tags$br(),
    logoList,
    tags$br(),
    fluid = FALSE
)
