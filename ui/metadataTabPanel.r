
metadataTabPanel <- tabPanel(
    value = "StepB",
    tagList(tags$span("(Step B)", class = "tabPanelSmall"), tags$br(), "Metadata", tags$br(), "Generator"),
    tags$fieldset(id = "metadataCleanDataFieldset", class = "singleLine",
        tags$legend("Step B.1: Upload dataset", class = "singleLine"),
        tags$div(class = "col-sm-3",
            textInput(
                inputId   = "metadataCleanDatasetName",
                label     = "Dataset name"
            )
        ),
        tags$div(class = "col-sm-3",
            fileInput(
                inputId   = "metadataCleanDataset",
                label     = tags$span("Dataset", class = "biggerFont"),
                accept    = acceptFileList()
            )
        ),
        tags$div(class = "col-sm-3",
            tags$span(class = "h5", "Import dataset"),
            actionButton(
                inputId   = "metadataImportAction",
                label     = tags$span("Import", class = "h4"),
                icon      = icon("open", lib = "glyphicon"),
                class     = "btn-primary btn-sm btn-block"
            )
        ),
        tags$div(class = "col-sm-3",
            tags$span(class = "h5", "Example dataset"),
            actionButton(
                inputId   = "metadataDemoAction",
                label     = tags$span("Demo", class = "h4"),
                class     = "btn-info btn-sm btn-block"
            )
        )
    ),
    tags$hr(),
    tags$fieldset(id = "metadataDatasetFieldset", class = "singleLine",
        tags$legend("Step B.2: Check dataset", class = "singleLine"),
        dataTableOutput(outputId = "metadataDatasetBrowser")
    ),
    tags$hr(),
    tags$h2("Fill in metatdata template"),
    tags$fieldset(id = "fillInOverallMetadataFieldset", class = "singleLine",
        tags$legend("Step B.3: Layer 1 (Standard Column)", class = "singleLine"),
        tags$div(class = "row",
            tags$div(class = "col-xs-12",
                tags$span(class = "h4", "Could not find time or event column? Use our "),
                tags$a(href = "/?tab=StepA",
                    tags$span(class = "h4 text-success", "Variable Appender")
                ),
                tags$span(class = "h4", ".")
            )
        ),
        uiOutput(outputId = "fillInOverallMetadataUI"),
        tags$div(class = "row",
            tags$div(class = "col-md-6 col-md-offset-3",
                uiOutput(outputId = "fillInOverallMetadataDownloadUI")
            )
        )
    ),
    tags$fieldset(id = "fillInVariableMetadataFieldset", class = "singleLine",
        tags$legend("Step B.4: Layer 2 (Variable Manager)", class = "singleLine"),
        tags$div(class = "row",
            tags$div(class = "col-xs-12",
                tags$span(class = "h4", "If you do not know the type of column, just keep it Undefined.")
            )
        ),
        uiOutput(outputId = "fillInVariableMetadataUI"),
        tags$div(class = "row",
            tags$div(class = "col-md-6 col-md-offset-3",
                uiOutput(outputId = "fillInVariableMetadataDownloadUI"),
                actionButton(
                    inputId   = "transferVariableTypeToLevelMetadata",
                    label     = tags$span("Transfer layer 2 type", class = "h4"),
                    class     = "btn btn-info btn-block"
                )
            )
        )
    ),
    tags$fieldset(id = "fillInLevelMetadataFieldset", class = "singleLine",
        tags$legend("Step B.5: Layer 3 (Categorical Reference)", class = "singleLine"),
        uiOutput(outputId = "fillInLevelMetadataUI"),
        tags$div(class = "row",
            tags$div(class = "col-md-6 col-md-offset-3",
                uiOutput(outputId = "fillInLevelMetadataDownloadUI")
            )
        )
    ),
    tags$hr(),
    tags$br(),
    logoList,
    tags$br(),
    fluid = FALSE
)
