
logoList <- tagList(
    tags$a(
        href = "http://www.ntu.edu.tw/english/index.html",
        target = "_blank",
        tags$img(src = "NTU.jpg", class = "logoImg")
    ),
    tags$a(
        href = "https://www.vghtpe.gov.tw/Index.action",
        target = "_blank",
        tags$img(src = "TVGH.png", class = "logoImg")
    )
)
