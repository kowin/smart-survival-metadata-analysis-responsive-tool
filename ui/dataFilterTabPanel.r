
dataFilterTabPanel <- tabPanel(
    value = "Step2",
    tagList(tags$span("Step 2", class = "h4"), tags$br(), "Data", tags$br(), "Filter"),
    tags$h3("Filter Option"),
    tags$div(class = "well",
        uiOutput(outputId = "uploadFilterOptions")
    ),
    tags$hr(),
    tags$div(class = "row",
        tags$div(class = "col-sm-6 col-md-4 col-lg-3",
            actionButton(
                inputId = "uploadFilterAction",
                label = tags$span("Filter", class = "h3"),
                icon = icon("filter"),
                class = "btn-primary btn-lg btn-block"
            )
        )
    ),
    tags$hr(),
    tags$h3("Filter History"),
    tableOutput(outputId = "uploadDatasetFilterCountTable"),
    tags$hr(),
    tags$fieldset(id = "uploadCleanDatasetFieldset", class = "singleLine",
        tags$legend("Standardized Dataset", class = "singleLine"),
        dataTableOutput(outputId = "uploadCleanDataset")
    ),
    tags$br(),
    logoList,
    tags$br()
)
