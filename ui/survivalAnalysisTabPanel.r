
survivalAnalysisSidebarPanel <- sidebarPanel(
    tags$fieldset(id = "uploadSurvivalFormulaOptionFieldset", class = "well",
        tags$legend("Survival Formula Options"),
        uiOutput(outputId = "uploadFactorOptionList"),
        uiOutput(outputId = "uploadTimeUnitOptionList")
    ),
    tags$fieldset(id = "uploadSurvivalCurveOptionFieldset", class = "well",
        tags$legend("Survival Curve Options"),
        uiOutput(outputId = "uploadSurvivalCurveOptionList")
    ),
    tags$fieldset(id = "uploadCoxTableOptionFieldset", class = "well",
        tags$legend("Cox Table Reference Category"),
        uiOutput(outputId = "uploadCoxTableOptionList")
    ),
    fluid = FALSE
)

survivalAnalysisTagList <- tagList(
    tags$div(class = "col-xs-12",
        buttonTypeRadioButton(
            inputId   = "survivalAnalysisShowSidebarPanel",
            label     = tags$h5("Option panel"),
            inline    = TRUE,
            selected  = "Show",
            choices   = c("Show", "Hide")
        )
    ),
    tags$h4(textOutput(outputId = "uploadSurvivalFormula", inline = TRUE)),
    tags$hr(),
    uiOutput(outputId = "uploadSurvivalCurveUI"),
    tags$hr(),
    tags$fieldset(id = "uploadCoxModelFieldset", class = "singleLine",
        tags$legend("Cox Proportional Hazards Model", class = "singleLine"),
        tags$h4(textOutput(outputId = "uploadUniVariateCoxModelTitle", inline = TRUE)),
        tableOutput(outputId = "uploadUniVariateCoxModelTable"),
        tags$h4(textOutput(outputId = "uploadMultiVariateCoxModelTitle", inline = TRUE)),
        tableOutput(outputId = "uploadMultiVariateCoxModelTable")
    ),
    tags$br(),
    logoList,
    tags$br()
)

survivalAnalysisTabPanel <- tabPanel(
    value = "Step4",
    tagList(tags$span("Step 4", class = "h4"), tags$br(), "Survival", tags$br(), "Analysis"),
    conditionalPanel("input.survivalAnalysisShowSidebarPanel == 'Show'",
        survivalAnalysisSidebarPanel
    ),
    survivalAnalysisTagList
)
