
uploadSidebarPanel <- sidebarPanel(
    actionButton(
        inputId   = "uploadDemoAnalysisAction",
        label     = tags$span("Demo", class = "h4"),
        class     = "btn-info  btn-block"
    ),
    tags$hr(),
    fileInput(
        inputId   = "uploadRawDataset",
        label     = tags$span("Dataset", class = "h4"),
        accept    = acceptFileList()
    ),
    tags$h4("Metadata"),
    fileInput(
        inputId   = "uploadOverallMetadata",
        label     = tags$span("Layer 1: Standard Column", class = "biggerFont"),
        accept    = acceptFileList()
    ),
    fileInput(
        inputId   = "uploadVariableMetadata",
        label     = tags$span("Layer 2: Variable Manager", class = "biggerFont"),
        accept    = acceptFileList()
    ),
    fileInput(
        inputId   = "uploadLevelMetadata",
        label     = tags$span("Layer 3: Categorical Reference", class = "biggerFont"),
        accept    = acceptFileList()
    ),
    actionButton(
        inputId   = "uploadStartAnalysisAction",
        label     = tags$span("Import", class = "h3"),
        icon      = icon("open", lib = "glyphicon"),
        class     = "btn-primary btn-lg btn-block"
    ),

    tags$hr(),

    div(class = "col-xs-12",
        tags$h4("Example Data")
    ),
    tags$br(),
    downloadButton(
        outputId  = "downloadExampleDataset",
        label = "Dataset"
    ),
    downloadButton(
        outputId  = "downloadExampleOverallMetadata",
        label = "Layer 1"
    ),
    downloadButton(
        outputId  = "downloadExampleVariableMetadata",
        label = "Layer 2"
    ),
    downloadButton(
        outputId  = "downloadExampleLevelMetadata",
        label = "Layer 3"
    ),
    tags$br(),
    fluid = FALSE
)
